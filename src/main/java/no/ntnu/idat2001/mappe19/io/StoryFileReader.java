package no.ntnu.idat2001.mappe19.io;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.actions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for reading in a .paths file as a Story object to be used in a game.
 */
public class StoryFileReader {
  private StoryFileReader() {}

  /**
   * Reads and parses the given .paths file, and returns the Story object corresponding
   * to the content in the file.
   *
   * @param filePath the filepath to the file to be parsed as a string.
   * @return the story object.
   * @throws IOException if the file doesn't exist, if the file has the wrong file type or
   * if the format in the file is wrong.
   */
  public static Story readFromFile(String filePath) throws IOException {
    if (!filePath.endsWith(".paths")) {
      throw new IOException("The provided file does not have the .paths filetype.");
    }
    Story story = null;
    String storyName;
    String fileContent;
    try {
      fileContent = Files.readString(Path.of(filePath), StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new IOException("Unable to read from given file.");
    }
    if (fileContent.startsWith("::")) {
      throw new IOException("The file does not start with the header.");
    }
    try (Scanner sc = new Scanner(fileContent)) {
      sc.useDelimiter("::");
      if (!sc.hasNext()) {
        throw new IOException("The file is empty.");
      }
      String fileHeader = sc.next();
      storyName = parseFileHeader(fileHeader);
      int passageNr = 0;
      while (sc.hasNext()) {
        Passage currentPassage;
        passageNr++;
        String passageString = sc.next();
        currentPassage = parsePassage(passageString, passageNr);
        if (passageNr == 1) {
          story = new Story(storyName, currentPassage);
        } else {
          story.addPassage(currentPassage);
        }
      }
      if (passageNr == 0) {
        throw new IOException("The story has no passages.");
      }
    }
    return story;
  }

  /**
   * Parses the given header String of the .paths file
   * and returns the name of the Story.
   *
   * @param fileHeader the header String.
   * @return the name of the Story as a String.
   * @throws IOException if the formatting of the header is wrong.
   */
  private static String parseFileHeader(String fileHeader) throws IOException {
    String storyName = null;
    try (Scanner storyScanner = new Scanner(fileHeader)) {
      storyScanner.useDelimiter("\n");
      storyName = storyScanner.next();
      if (!storyScanner.hasNext()) {
        throw new IOException("There is no newline after the name of the story.");
      }
      String emptyLine = storyScanner.next();
      if (!emptyLine.equals("")) {
        throw new IOException("The formatting between the story name" +
            " and the first passage is wrong.");
      }
    } catch (NoSuchElementException e) {
      throw new IOException("Error parsing story name");
    }
    return storyName;
  }

  /**
   * Parses a given Link String from the file
   * and returns a Link object from the parsed data.
   *
   * @param linkString the Link String from the file contents.
   * @param passageNr the number of the passage the link belongs to, used in an error message.
   * @return the Link object.
   * @throws IOException if the formatting of the Link String was wrong.
   */
  private static Link parseLink(String linkString, int passageNr) throws IOException {
    try {
      Pattern p = Pattern.compile("\\((.+)\\)\\[(.+)]");
      Matcher m = p.matcher(linkString);
      if (m.find()) {
        return new Link(m.group(1), m.group(2));
      } else {
        throw new NoSuchElementException();
      }
    } catch (Exception e) {
      throw new IOException("One of the links being added to passage nr. " + passageNr
          + " was wrong.");
    }
  }

  /**
   * Parses a given Action String from the file
   * and returns an Action object from the parsed data.
   *
   * @param actionString the Action String from the file contents.
   * @param passageNr the number of the passage the Action belongs to, used in an error message.
   * @return the Action object.
   * @throws IOException if the formatting of the Action String was wrong.
   */
  private static Action parseAction(String actionString, int passageNr) throws IOException {
    try {
      Pattern p = Pattern.compile("- (.+), value: (.+)");
      Matcher m = p.matcher(actionString);
      String actionType = "";
      if (m.find()) {
        actionType = m.group(1);
      }
      switch (actionType) {
        case ("gold_action") -> {
          return new GoldAction(Integer.parseInt(m.group(2)));
        }
        case ("health_action") -> {
          return new HealthAction(Integer.parseInt(m.group(2)));
        }
        case ("score_action") -> {
          return new ScoreAction(Integer.parseInt(m.group(2)));
        }
        case ("inventory_action") -> {
          return new InventoryAction(m.group(2));
        }
        default -> throw new IllegalArgumentException();
      }
    } catch (Exception e) {
      throw new IOException("One of the actions being added to a link in passage nr "
          + passageNr + " was wrong.");
    }
  }

  /**
   * Parses a given Passage String from the file
   * and returns a Passage object from the parsed data.
   *
   * @param passageString the Passage String from the file contents.
   * @param passageNr the number of the passage, used in an error message.
   * @return the Passage object.
   * @throws IOException if the formatting of the Passage String was wrong.
   */
  private static Passage parsePassage(String passageString, int passageNr) throws IOException {
    Passage currentPassage;
    if (!passageString.endsWith("\n\n")) {
      throw new IOException("Passage block did not end with empty line.");
    }
    //Ensure reader can properly throw when passage is not properly defined.
    passageString = passageString.trim();
    try (Scanner passageScanner = new Scanner(passageString)) {
      passageScanner.useDelimiter("\n");
      currentPassage = new Passage(passageScanner.next(), passageScanner.next());
      Link currentLink = null;
      while (passageScanner.hasNext()) {
        String currentString = passageScanner.next();
        if (currentString.startsWith("(") && currentString.endsWith("]")) {
          currentLink = parseLink(currentString, passageNr);
          currentPassage.addLink(currentLink);
        } else if (currentString.startsWith("-")) {
          if (currentLink == null) {
            throw new IOException("An action was specified before a link in passage nr "
                + passageNr + ".");
          }
          currentLink.addAction(parseAction(currentString, passageNr));
        } else if (!currentString.equals("")){
          throw new IOException("An illegal line was found while parsing passage nr. "
              + passageNr + ".");
        }
      }
    } catch (NoSuchElementException e) {
      throw new IOException("Error parsing passage nr. " + passageNr + ".");
    }
    return currentPassage;
  }
}

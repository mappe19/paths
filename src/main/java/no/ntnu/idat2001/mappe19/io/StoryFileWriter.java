package no.ntnu.idat2001.mappe19.io;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.actions.Action;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Helper class used for writing a Story object to file.
 * The story is written as plain text in a specific format
 * to a file with the .paths file ending.
 * The Story class, its constituent passages, the passages' links
 * and the links' actions are all written to the file.
 */
public class StoryFileWriter {
  /**
   * Private constructor because the class does not need
   * to be instanced.
   */
  private StoryFileWriter() {}

  /**
   * Writes a given Story object to the given file path.
   * The file must have the ending .paths.
   *
   * @param filePath the path to the file as a string.
   * @param story the Story object to write to file.
   * @throws IOException if there is an error writing to the given file,
   * or if the file name is invalid.
   * @throws IllegalArgumentException if the given Story object is null.
   */
  public static void writeToFile(String filePath, Story story) throws IOException, IllegalArgumentException {
    if (story == null) {
      throw new IllegalArgumentException("Story cannot be null");
    }
    if (!filePath.endsWith(".paths")) {
      throw new IOException("File must end with .paths");
    }
    try (FileWriter fileWriter = new FileWriter(filePath)) {
      fileWriter.write(story.getTitle() + "\n\n");

      //Ensure opening passage is written as first passage.
      Passage openingPassage = story.getOpeningPassage();
      fileWriter.write("::" + openingPassage.getTitle() + "\n");
      fileWriter.write(openingPassage.getContent() + "\n");
      for (Link link : openingPassage.getLinks()) {
        fileWriter.write("(" + link.getText() + ")[" + link.getReference() + "]\n");
        for (Action action : link.getActions()) {
          fileWriter.write(action.toString() + "\n");
        }
      }
      fileWriter.write("\n");

      //Write all passages afterwards.
      for (Passage passage : story.getPassages()) {
        if (!passage.equals(openingPassage)) {
          fileWriter.write("::" + passage.getTitle() + "\n");
          fileWriter.write(passage.getContent() + "\n");
          for (Link link : passage.getLinks()) {
            fileWriter.write("(" + link.getText() + ")[" + link.getReference() + "]\n");
            for (Action action : link.getActions()) {
              fileWriter.write(action.toString() + "\n");
            }
          }
          fileWriter.write("\n");
        }
      }
    }
  }
}

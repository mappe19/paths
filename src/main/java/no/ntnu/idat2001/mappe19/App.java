package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.view.PathsApp;

/**
 * Class used to start the application.
 */
public class App {

  /**
   * The main method called to launch the application.
   *
   * @param args command line arguments passed to the application.
   */
  public static void main(String[] args) {
    PathsApp.main(args);
  }
}

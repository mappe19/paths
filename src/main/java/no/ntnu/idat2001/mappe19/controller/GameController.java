package no.ntnu.idat2001.mappe19.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idat2001.mappe19.model.Game;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Player;
import no.ntnu.idat2001.mappe19.model.actions.Action;
import no.ntnu.idat2001.mappe19.view.GameView;

/**
 * Controller class for the GameView class.
 * Handles displaying and updating the view, and contains the methods triggered by the buttons.
 */
public class GameController {
  private Game game;
  private final GameView view;
  private final Stage stage;
  private final Stage helpStage;
  private Passage currentPassage;
  private final int initialPlayerHealth;
  private final int initialPlayerGold;
  private final int initialPlayerScore;

  /**
   * Constructs a GameController object with a Game object
   * containing all information used for playing a game
   * and a Stage object used for displaying
   * the GameView object created by an object of this class.
   *
   * @param game the Game object representing the game being played.
   * @param stage the Stage object used for displaying te GUI.
   */
  public GameController(Game game, Stage stage) {
    this.game = game;
    this.stage = stage;
    this.helpStage = new Stage();
    this.helpStage.setResizable(false);
    this.view = new GameView();
    this.currentPassage = game.begin();
    this.initialPlayerHealth = game.getPlayer().getHealth();
    this.initialPlayerGold = game.getPlayer().getGold();
    this.initialPlayerScore = game.getPlayer().getScore();

    setupViewButtons();
  }

  /**
   * Displays the GUI elements controlled by objects of the GameController class.
   */
  public void showContent() {
    stage.setScene(new Scene(view, 700, 700));
    view.update(game.getPlayer(), currentPassage, createLinkButtons());
    if (game.getPlayer().getHealth() == 0) {
      showDeathAlert();
    }
    stage.show();
  }

  /**
   * Creates an ArrayList object containing Button objects
   * representing all Links in the current Passage.
   * The buttons are used for navigating between Passages, and are passed to the view object
   * for handling the layout of the buttons.
   *
   * @return the ArrayList containing the Button objects.
   */
  private List<Button> createLinkButtons() {
    ArrayList<Button> buttons = new ArrayList<>();

    currentPassage.getLinks().forEach(link -> {
      if (!game.getStory().getBrokenLinks().contains(link)) {
        Button linkButton = new Button(link.getText());

        linkButton.setOnAction(actionEvent -> {
          for (Action action : link.getActions()) {
            action.execute(game.getPlayer());
          }
          currentPassage = game.go(link);
          Player player = game.getPlayer();
          view.update(player, currentPassage, createLinkButtons());
          if (player.getHealth() == 0) {
            showDeathAlert();
          }
        });

        buttons.add(linkButton);
      }
    });

    return buttons;
  }

  /**
   * Binds methods to the buttons created by the GameView object in a GameController object.
   */
  private void setupViewButtons() {
    view.getSeeGoalsStatusBtn().setOnAction(actionEvent -> showGoalsAlert());
    view.getSeeInventoryBtn().setOnAction(actionEvent -> showInventoryAlert());
    view.getEndGameButton().setOnAction(actionEvent -> showEndOfGameDialog());
    view.getRestartGameButton().setOnAction(actionEvent -> showRestartGameDialog());
    view.getHelpButton().setOnAction(actionEvent -> showHelpScreen());
  }

  /**
   * Displays an alert to the player indicating they have died.
   * Triggers the end of game dialog after the death alert has
   * been dismissed.
   */
  private void showDeathAlert() {
    Alert deathAlert = new Alert(Alert.AlertType.INFORMATION);
    deathAlert.setTitle("End of game");
    deathAlert.setHeaderText("You have died.");
    deathAlert.setContentText("Your health has reached 0.");
    deathAlert.showAndWait();
    showEndOfGameDialog();
  }

  /**
   * Shows an alert informing the player that the game has ended.
   * Summarises the game, and gives the player the ability to see the final status of goals,
   * inventory, health, gold and score.
   * Also presents the user with the ability to return to the game setup GUI.
   */
  private void showEndOfGameDialog() {
    Alert endOfGameAlert = new Alert(Alert.AlertType.CONFIRMATION);
    endOfGameAlert.setTitle("End of game");
    endOfGameAlert.setHeaderText("Your game has ended.");
    endOfGameAlert.setContentText("Your playthrough of the story with the title "
        + game.getStory().getTitle() + " has ended."
        + "\nThe player by the name of " + game.getPlayer().getName() + " ended their journey\n"
        + " with " + game.getPlayer().getHealth() + " health, "
        + game.getPlayer().getGold() + " gold and a final score of "
        + game.getPlayer().getScore() + " points.");

    ButtonType buttonTypeSeeGoals = new ButtonType("See goals");
    ButtonType buttonTypeSeeInventory = new ButtonType("See inventory");
    ButtonType buttonTypeCancel = new ButtonType("Go back to game setup",
        ButtonBar.ButtonData.CANCEL_CLOSE);

    endOfGameAlert.getButtonTypes().setAll(buttonTypeSeeGoals,
        buttonTypeSeeInventory,
        buttonTypeCancel);

    Optional<ButtonType> result = endOfGameAlert.showAndWait();
    while (result.isPresent()) {
      if (result.get() == buttonTypeSeeGoals) {
        showGoalsAlert();
        result = endOfGameAlert.showAndWait();
      } else if (result.get() == buttonTypeSeeInventory) {
        showInventoryAlert();
        result = endOfGameAlert.showAndWait();
      } else {
        break;
      }
    }

    GameSetupController gameSetupController = new GameSetupController(stage);
    gameSetupController.showContent();
  }

  /**
   * Shows the window describing the players current inventory.
   */
  private void showInventoryAlert() {
    String message = "There are no items in the inventory.";
    List<String> items = game.getPlayer().getInventory();

    if (!items.isEmpty()) {
      StringBuilder itemsMessage = new StringBuilder("Items: ");
      int indentation = itemsMessage.length();
      items.forEach(item -> {
        itemsMessage.append(item);
        if (items.indexOf(item) == items.size() - 1) {
          itemsMessage.append(".");
        } else if (items.indexOf(item) % 5 == 0) {
          itemsMessage.append(",\n").append(" ".repeat(indentation));
        } else {
          itemsMessage.append(", ");
        }
      });
      message = itemsMessage.toString();
    }

    Alert inventoryAlert = new Alert(Alert.AlertType.INFORMATION);
    inventoryAlert.setTitle("Inventory");
    inventoryAlert.setHeaderText("The items currently in the inventory.");
    inventoryAlert.setContentText(message);
    inventoryAlert.showAndWait();
  }

  /**
   * Shows the window describing the current status of the player's goals.
   */
  private void showGoalsAlert() {
    String message = "There are no goals registered.";

    if (!game.getGameGoals().isEmpty()) {
      StringBuilder goalsMessage = new StringBuilder();
      game.getGameGoals().forEach(goal -> {
        goalsMessage.append(goal).append("\n");
        if (goal.isFulfilled(game.getPlayer())) {
          goalsMessage.append("Fulfilled\n\n");
        } else {
          goalsMessage.append("Not fulfilled\n\n");
        }
      });
      message = goalsMessage.toString();
    }

    Alert goalsAlert = new Alert(Alert.AlertType.INFORMATION);
    goalsAlert.setTitle("Goals");
    goalsAlert.setHeaderText("The goals for this game.");
    goalsAlert.setContentText(message);
    goalsAlert.showAndWait();
  }

  /**
   * Presents the user with a confirmation dialog when the user
   * clicks the "Restart game" button, and resets the game if the player confirms.
   */
  private void showRestartGameDialog() {
    Alert restartGameAlert = new Alert(Alert.AlertType.CONFIRMATION);
    restartGameAlert.setTitle("Restart game");
    restartGameAlert.setContentText("Are you sure you want to restart the game?");
    Optional<ButtonType> result = restartGameAlert.showAndWait();

    if (result.isPresent() && result.get() == ButtonType.OK) {
      game = new Game(new Player.PlayerBuilder(game.getPlayer().getName())
          .setGold(initialPlayerGold)
          .setHealth(initialPlayerHealth)
          .setScore(initialPlayerScore)
          .build(), game.getStory(), game.getGameGoals());
      currentPassage = game.begin();
      view.update(game.getPlayer(), currentPassage, createLinkButtons());
      view.getEndGameButton().setVisible(false);
    }
  }

  /**
   * Presents the user with a window containing instructions for playing a game
   * when the "Help" button is clicked.
   */
  private void showHelpScreen() {
    if (!helpStage.isShowing()) {
      HBox helpContainer = new HBox();
      helpContainer.setPadding(new Insets(10));
      Text helpText = new Text();
      helpContainer.getChildren().add(helpText);

      helpText.setText("""
                    How to play:
                    The top of the game window shows the players name,
                    current health, current score and current gold amount.
                    The top of the game screen also contains the buttons for viewing the inventory and the goals
                    that were set by the player before the game.
                    The window showing the goals set will also indicate if each goal has been fulfilled or not.
                    The middle of the screen will display the title and content of the current passage of the story
                    that is being played.
                    It will also display all possible actions to take shown as buttons. If there are none, the GUI
                    will indicate this, and it will be possible to end the game by clicking the button at the bottom
                    of the screen.
                    When a button specifying an action to take is clicked, the screen will update to show the next
                    passage, and the players status will update if the action taken changed it in some way.
                    
                    To restart a game, the player can click the restart game button at the bottom of the screen,
                    and will be prompted for confirmation to restart the game. Restarting the game, will set the screen
                    to display the first passage of the story, and will reset the players status to the values specified
                    when starting the game.
                    """);

      Scene helpScene = new Scene(helpContainer, 700, 500);
      helpStage.setScene(helpScene);
      helpStage.show();
    }
  }
}

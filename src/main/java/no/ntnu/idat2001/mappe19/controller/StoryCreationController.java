package no.ntnu.idat2001.mappe19.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import no.ntnu.idat2001.mappe19.io.StoryFileWriter;
import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.actions.GoldAction;
import no.ntnu.idat2001.mappe19.model.actions.HealthAction;
import no.ntnu.idat2001.mappe19.model.actions.InventoryAction;
import no.ntnu.idat2001.mappe19.model.actions.ScoreAction;
import no.ntnu.idat2001.mappe19.view.StoryCreationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Controller class for the StoryCreationView class.
 * Handles displaying and updating the view, and contains the methods triggered by the buttons.
 */
public class StoryCreationController {
  private final String ADD_ACTION_DIALOG_TITLE = "Add action";
  private final StoryCreationView view;
  private final Stage stage;
  private final ArrayList<Passage> passages;

  /**
   * Constructs a StoryCreationController object with a Stage object
   * used to display the StoryCreationView created by an object of this class.
   *
   * @param stage the Stage object used to display the view.
   */
  public StoryCreationController(Stage stage) {
    this.stage = stage;
    this.view = new StoryCreationView();
    this.passages = new ArrayList<>();

    setupButtons();
  }

  /**
   * Displays the GUI elements controlled by objects of the StoryCreationController class.
   */
  public void showContent() {
    stage.setScene(new Scene(view, 800, 800));
    view.updateStoryContent(passages);
    stage.show();
  }

  /**
   * Binds methods to the buttons created by the StoryCreationView object in a StoryCreationController object.
   */
  private void setupButtons() {
    view.getSaveStoryBtn().setOnAction(actionEvent -> saveStory());
    view.getAddPassageBtn().setOnAction(actionEvent -> addPassage());
    view.getAddLinkBtn().setOnAction(actionEvent -> addLink());
    view.getAddActionBtn().setOnAction(actionEvent -> choosePassageAndLinkToAddActionTo());
    view.getCancelButton().setOnAction(actionEvent -> cancelCreationOfStory());
  }

  /**
   * Shows dialog for confirming to save the created story,
   * and saves the story if confirmed.
   */
  private void saveStory() {
    Alert confirmaSaveStoryAlert = new Alert(Alert.AlertType.CONFIRMATION);
    confirmaSaveStoryAlert.setTitle("Save story");
    confirmaSaveStoryAlert.setContentText("Are you sure you want to save the story of the story?");
    Optional<ButtonType> result = confirmaSaveStoryAlert.showAndWait();

    if (result.isPresent() && result.get() == ButtonType.OK) {
      try {
        Passage openingPassage = null;
        if (!passages.isEmpty()) {
          openingPassage = passages.get(0);
        }

        Story story = new Story(view.getStoryTitleFieldText(), openingPassage);
        if (passages.size() > 1) {
          for (int i = 1; i < passages.size(); i++) {
            story.addPassage(passages.get(i));
          }
        }
        StoryFileWriter.writeToFile(view.getFilePathFieldText(), story);
        GameSetupController gameSetupController = new GameSetupController(stage);
        gameSetupController.showContent();
      } catch (IllegalArgumentException e) {
        String contextText;
        if (e.getMessage().equals("The openingPassage parameter can not be null.")) {
          contextText = "You must specify a beginning passage";
        } else {
          contextText = e.getMessage();
        }
        Alert errorCreatingStoryAlert = new Alert(Alert.AlertType.ERROR);
        errorCreatingStoryAlert.setTitle("Error creating story");
        errorCreatingStoryAlert.setHeaderText("An error occurred when trying to save the story");
        errorCreatingStoryAlert.setContentText(contextText);
        errorCreatingStoryAlert.showAndWait();
      } catch (IOException e) {
        Alert errrorSavingToFileAlert = new Alert(Alert.AlertType.ERROR);
        errrorSavingToFileAlert.setTitle("Error saving to file");
        errrorSavingToFileAlert.setHeaderText("An error occurred when trying to save the story");
        errrorSavingToFileAlert.setContentText(e.getMessage());
        errrorSavingToFileAlert.showAndWait();
      }
    }
  }

  /**
   * Shows dialogs for adding a new Passage, and adds the specified Passage if the process is completed.
   */
  private void addPassage() {
    final String ADD_PASSAGE_DIALOG_TITLE = "Add passage";

    TextInputDialog passageTitleDialog = new TextInputDialog();
    if (passages.isEmpty()) {
      Alert informIsOpeningPassageAlert = new Alert(Alert.AlertType.INFORMATION);
      informIsOpeningPassageAlert.setTitle(ADD_PASSAGE_DIALOG_TITLE);
      informIsOpeningPassageAlert.setContentText("As this is the first passage that will be added," +
          " it will be used as the opening passage of the story.");
      informIsOpeningPassageAlert.showAndWait();
    }
    passageTitleDialog.setTitle(ADD_PASSAGE_DIALOG_TITLE);
    passageTitleDialog.setHeaderText("Input the title of passage");
    passageTitleDialog.setContentText("The title of the passage:");
    Optional<String> passageTitleResult = passageTitleDialog.showAndWait();
    if (passageTitleResult.isPresent()) {
      TextInputDialog passageContentDialog = new TextInputDialog();
      passageContentDialog.setTitle(ADD_PASSAGE_DIALOG_TITLE);
      passageContentDialog.setHeaderText("Input the content of the passage");
      passageContentDialog.setContentText("The content of the passage:");
      Optional<String> passageContentResult = passageContentDialog.showAndWait();
      if (passageContentResult.isPresent()) {
        Passage passage = new Passage(passageTitleResult.get(), passageContentResult.get());
        passages.add(passage);
        view.updateStoryContent(passages);
      }
    }
  }

  /**
   * Shows dialogs for adding a new Link, and adds the specified Link if the process is completed.
   * The process include specifying the Passage to add the Link to.
   */
  private void addLink() {
    final String ADD_LINK_DIALOG_TITLE = "Add link";

    if (passages.isEmpty()) {
      Alert informIsOpeningPassageAlert = new Alert(Alert.AlertType.WARNING);
      informIsOpeningPassageAlert.setTitle(ADD_LINK_DIALOG_TITLE);
      informIsOpeningPassageAlert.setContentText("There are no passages, so it is not possible to add a link.");
      informIsOpeningPassageAlert.showAndWait();
    } else {
      List<String> passageChoices = new ArrayList<>();
      AtomicInteger counter = new AtomicInteger(1);

      passages.forEach(passage -> {
        passageChoices.add(counter + ". Title: " + passage.getTitle()
            + ", Content: " + passage.getContent());
        counter.getAndIncrement();
      });

      ChoiceDialog<String> passageChoiceDialog = new ChoiceDialog<>(passageChoices.get(0), passageChoices);
      passageChoiceDialog.setTitle(ADD_LINK_DIALOG_TITLE);
      passageChoiceDialog.setHeaderText("Specify the passage to add the link to");
      passageChoiceDialog.setContentText("Choose the passage:");

      Optional<String> passageChoiceResult = passageChoiceDialog.showAndWait();
      if (passageChoiceResult.isPresent()) {
        try {
          int indexOfChosenPassage = Integer.parseInt(passageChoiceResult.get().substring(0, 1)) - 1;
          Passage selectedPassage = passages.get(indexOfChosenPassage);

          TextInputDialog linkTextDialog = new TextInputDialog();
          linkTextDialog.setTitle(ADD_LINK_DIALOG_TITLE);
          linkTextDialog.setHeaderText("Specify the display text of the link.");
          linkTextDialog.setContentText("Write the link text:");

          TextField linkTextDialogField = linkTextDialog.getEditor();
          BooleanBinding linkTextIsInvalid = Bindings.createBooleanBinding(
              () -> linkTextDialogField.getText().equals(""),
              linkTextDialogField.textProperty());
          linkTextDialog.getDialogPane()
              .lookupButton(ButtonType.OK)
              .disableProperty()
              .bind(linkTextIsInvalid);

          Optional<String> linkTextResult = linkTextDialog.showAndWait();
          if (linkTextResult.isPresent()) {
            TextInputDialog linkReferenceDialog = new TextInputDialog();
            linkReferenceDialog.setTitle(ADD_LINK_DIALOG_TITLE);
            linkReferenceDialog.setHeaderText("Specify the reference of the link." +
                "\nFor the link to be valid, the reference bust be equal to a passage title.");
            linkReferenceDialog.setContentText("Write the link reference:");

            TextField linkReferenceDialogField = linkReferenceDialog.getEditor();
            BooleanBinding linkReferenceIsInvalid = Bindings.createBooleanBinding(
                () -> linkReferenceDialogField.getText().equals(""),
                linkReferenceDialogField.textProperty()
            );
            linkReferenceDialog.getDialogPane()
                .lookupButton(ButtonType.OK)
                .disableProperty()
                .bind(linkReferenceIsInvalid);

            Optional<String> linkReferenceResult = linkReferenceDialog.showAndWait();
            linkReferenceResult.ifPresent(s -> selectedPassage.addLink(new Link(linkTextResult.get(), s)));
            view.updateStoryContent(passages);
          }
        } catch (Exception e) {
          Alert errorParsingSelectedPassageAlert = new Alert(Alert.AlertType.ERROR);
          errorParsingSelectedPassageAlert.setTitle(ADD_LINK_DIALOG_TITLE);
          errorParsingSelectedPassageAlert.setHeaderText("Error parsing selected passage.");
          errorParsingSelectedPassageAlert.setContentText("An error occurred while trying to parse" +
              " the selected passage.\nThe process of adding a link will be cancelled.");
          errorParsingSelectedPassageAlert.showAndWait();
        }
      }
    }
  }

  /**
   * Shows dialogs for specifying the Passage and the Link in the Passage to add an Action to.
   * The method passes these selections on to the methods for adding an Action.
   */
  private void choosePassageAndLinkToAddActionTo() {
    if (passages.isEmpty()) {
      Alert informIsOpeningPassageAlert = new Alert(Alert.AlertType.WARNING);
      informIsOpeningPassageAlert.setTitle(ADD_ACTION_DIALOG_TITLE);
      informIsOpeningPassageAlert.setContentText("There are no passages, so it is not possible to add an action.");
      informIsOpeningPassageAlert.showAndWait();
    } else {
      List<String> passageChoices = new ArrayList<>();
      AtomicInteger passageCounter = new AtomicInteger(1);

      passages.forEach(passage -> {
        passageChoices.add(passageCounter + ". Title: " + passage.getTitle()
            + ", Content: " + passage.getContent());
        passageCounter.getAndIncrement();
      });

      ChoiceDialog<String> passageChoiceDialog = new ChoiceDialog<>(passageChoices.get(0), passageChoices);
      passageChoiceDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
      passageChoiceDialog.setHeaderText("Specify the passage to select the link from");
      passageChoiceDialog.setContentText("Choose the passage:");

      Optional<String> passageChoiceResult = passageChoiceDialog.showAndWait();
      if (passageChoiceResult.isPresent()) {
        try {
          int indexOfChosenPassage = Integer.parseInt(passageChoiceResult.get().substring(0, 1)) - 1;
          Passage selectedPassage = passages.get(indexOfChosenPassage);

          if (selectedPassage.hasLinks()) {
            List<String> linkChoices = new ArrayList<>();
            AtomicInteger linkCounter = new AtomicInteger(1);

            selectedPassage.getLinks().forEach(link -> {
              linkChoices.add(linkCounter + ". Text: " + link.getText()
                  + ", Reference: " + link.getReference());
              linkCounter.getAndIncrement();
            });

            ChoiceDialog<String> linkChoiceDialog = new ChoiceDialog<>(linkChoices.get(0), linkChoices);
            linkChoiceDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
            linkChoiceDialog.setHeaderText("Specify the link to add the action to");
            linkChoiceDialog.setContentText("Choose the link:");

            Optional<String> linkChoiceResult = linkChoiceDialog.showAndWait();
            linkChoiceResult.ifPresent(s -> specifyActionType(s, selectedPassage));
          } else {
            Alert informNoLinksAlert = new Alert(Alert.AlertType.WARNING);
            informNoLinksAlert.setTitle(ADD_ACTION_DIALOG_TITLE);
            informNoLinksAlert.setContentText("There are no links in this passage," +
                " so it is not possible to add an action.");
            informNoLinksAlert.showAndWait();
          }
        } catch (Exception e) {
          Alert errorParsingSelectedPassageAlert = new Alert(Alert.AlertType.ERROR);
          errorParsingSelectedPassageAlert.setTitle(ADD_ACTION_DIALOG_TITLE);
          errorParsingSelectedPassageAlert.setHeaderText("Error parsing selected passage.");
          errorParsingSelectedPassageAlert.setContentText("An error occurred while trying to parse" +
              " the selected passage.\nThe process of adding an action will be cancelled.");
          errorParsingSelectedPassageAlert.showAndWait();
        }
      }
    }
  }

  /**
   * Shows dialog for specifying the type of Action to add,
   * and triggers the corresponding method for adding the specified Action type.
   *
   * @param linkChoiceResultString the String specifying the link to add the action to.
   * @param selectedPassage the Passage object specifying the Passage the selected Link is in.
   */
  private void specifyActionType(String linkChoiceResultString, Passage selectedPassage) {
    final String HEALTH_ACTION_CHOICE_STRING = "Health Action";
    final String SCORE_ACTION_CHOICE_STRING = "Score Action";
    final String GOLD_ACTION_CHOICE_STRING = "Gold Action";
    final String INVENTORY_ACTION_CHOICE_STRING = "Inventory Action";

    try {
      int indexOfChosenLink = Integer.parseInt(linkChoiceResultString.substring(0, 1)) - 1;
      Link selectedLink = selectedPassage.getLinks().get(indexOfChosenLink);

      ArrayList<String> actionTypeChoices = new ArrayList<>();
      actionTypeChoices.add(HEALTH_ACTION_CHOICE_STRING);
      actionTypeChoices.add(SCORE_ACTION_CHOICE_STRING);
      actionTypeChoices.add(GOLD_ACTION_CHOICE_STRING);
      actionTypeChoices.add(INVENTORY_ACTION_CHOICE_STRING);

      ChoiceDialog<String> actionTypeDialog = new ChoiceDialog<>(actionTypeChoices.get(0),
          actionTypeChoices);
      actionTypeDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
      actionTypeDialog.setHeaderText("Choose the type of action to add");
      actionTypeDialog.setContentText("Action type:");
      Optional<String> actionTypeResult = actionTypeDialog.showAndWait();

      if (actionTypeResult.isPresent()) {
        switch (actionTypeResult.get()) {
          case (HEALTH_ACTION_CHOICE_STRING) -> addHealthAction(selectedLink);
          case (GOLD_ACTION_CHOICE_STRING) -> addGoldAction(selectedLink);
          case (SCORE_ACTION_CHOICE_STRING) -> addScoreAction(selectedLink);
          case (INVENTORY_ACTION_CHOICE_STRING) -> addInventoryAction(selectedLink);
          default -> {
            Alert errorParsingSelectedPassageAlert = new Alert(Alert.AlertType.ERROR);
            errorParsingSelectedPassageAlert.setTitle(ADD_ACTION_DIALOG_TITLE);
            errorParsingSelectedPassageAlert.setHeaderText("Error parsing selected action type.");
            errorParsingSelectedPassageAlert.setContentText("An error occurred while trying to parse" +
                " the selected action type.\nThe process of adding an action will be cancelled.");
          }
        }
        view.updateStoryContent(passages);
      }
    } catch (Exception e) {
      Alert errorParsingSelectedPassageAlert = new Alert(Alert.AlertType.ERROR);
      errorParsingSelectedPassageAlert.setTitle(ADD_ACTION_DIALOG_TITLE);
      errorParsingSelectedPassageAlert.setHeaderText("Error parsing selected link.");
      errorParsingSelectedPassageAlert.setContentText("An error occurred while trying to parse" +
          " the selected link.\nThe process of adding an action will be cancelled.");
    }
  }

  /**
   * Shows dialog for specifying the content of a HealthAction
   * to be added to the specified Link, and adds the action to the Link
   * if the process is completed.
   *
   * @param selectedLink the Link object representing the specified Link.
   */
  private void addHealthAction(Link selectedLink) {
    TextInputDialog healthAmountDialog = new TextInputDialog();
    healthAmountDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
    healthAmountDialog.setHeaderText("Specify the health amount of the action.");
    healthAmountDialog.setContentText("Health amount (integer):");

    TextField healthAmountDialogField = healthAmountDialog.getEditor();
    BooleanBinding isInvalidHealthAmount = Bindings.createBooleanBinding(
        () -> isNotInteger(healthAmountDialogField.getText()),
        healthAmountDialogField.textProperty()
    );
    healthAmountDialog.getDialogPane()
        .lookupButton(ButtonType.OK)
        .disableProperty()
        .bind(isInvalidHealthAmount);

    Optional<String> healthAmountResult = healthAmountDialog.showAndWait();
    healthAmountResult.ifPresent(s -> selectedLink.addAction(
        new HealthAction(Integer.parseInt(s))
    ));
  }

  /**
   * Shows dialog for specifying the content of a GoldAction
   * to be added to the specified Link, and adds the action to the Link
   * if the process is completed.
   *
   * @param selectedLink the Link object representing the specified Link.
   */
  private void addGoldAction(Link selectedLink) {
    TextInputDialog goldAmountDialog = new TextInputDialog();
    goldAmountDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
    goldAmountDialog.setHeaderText("Specify the gold amount of the action.");
    goldAmountDialog.setContentText("Gold amount (integer):");

    TextField goldAmountDialogField = goldAmountDialog.getEditor();
    BooleanBinding isInvalidGoldAmount = Bindings.createBooleanBinding(
        () -> isNotInteger(goldAmountDialogField.getText()),
        goldAmountDialogField.textProperty()
    );
    goldAmountDialog.getDialogPane()
        .lookupButton(ButtonType.OK)
        .disableProperty()
        .bind(isInvalidGoldAmount);

    Optional<String> goldAmountResult = goldAmountDialog.showAndWait();
    goldAmountResult.ifPresent(s -> selectedLink.addAction(
        new GoldAction(Integer.parseInt(s))
    ));
  }

  /**
   * Shows dialog for specifying the content of a ScoreAction
   * to be added to the specified Link, and adds the action to the Link
   * if the process is completed.
   *
   * @param selectedLink the Link object representing the specified Link.
   */
  private void addScoreAction(Link selectedLink) {
    TextInputDialog scoreAmountDialog = new TextInputDialog();
    scoreAmountDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
    scoreAmountDialog.setHeaderText("Specify the score amount of the action.");
    scoreAmountDialog.setContentText("Score amount (integer):");

    TextField scoreAmountDialogField = scoreAmountDialog.getEditor();
    BooleanBinding isInvalidScoreAmount = Bindings.createBooleanBinding(
        () -> isNotInteger(scoreAmountDialogField.getText()),
        scoreAmountDialogField.textProperty()
    );
    scoreAmountDialog.getDialogPane()
        .lookupButton(ButtonType.OK)
        .disableProperty()
        .bind(isInvalidScoreAmount);

    Optional<String> scoreAmountResult = scoreAmountDialog.showAndWait();
    scoreAmountResult.ifPresent(s -> selectedLink.addAction(
        new ScoreAction(Integer.parseInt(s))
    ));
  }

  /**
   * Shows dialog for specifying the content of a InventoryAction
   * to be added to the specified Link, and adds the action to the Link
   * if the process is completed.
   *
   * @param selectedLink the Link object representing the specified Link.
   */
  private void addInventoryAction(Link selectedLink) {
    TextInputDialog itemDialog = new TextInputDialog();
    itemDialog.setTitle(ADD_ACTION_DIALOG_TITLE);
    itemDialog.setHeaderText("Specify the item to be added by the action.");
    itemDialog.setContentText("Item:");

    TextField itemDialogField = itemDialog.getEditor();
    BooleanBinding isInvalidScoreAmount = Bindings.createBooleanBinding(
        () -> itemDialogField.getText().equals(""),
        itemDialogField.textProperty()
    );
    itemDialog.getDialogPane()
        .lookupButton(ButtonType.OK)
        .disableProperty()
        .bind(isInvalidScoreAmount);

    Optional<String> itemResult = itemDialog.showAndWait();
    itemResult.ifPresent(s -> selectedLink.addAction(
        new InventoryAction(s)
    ));
  }

  /**
   * Checks if a given String only contains an integer.
   * Used for disabling or enabling the "OK" button
   * ni the dialogs for specifying the amount of
   * an HealthAction, GoldAction or ScoreAction.
   * This prevents illegal values.
   *
   * @param s the String to check.
   * @return true if the String only contains an integer, false if not.
   */
  private boolean isNotInteger(String s) {
    try {
      Integer.parseInt(s);
      return false;
    } catch (NumberFormatException e) {
      return true;
    }
  }

  /**
   * Shows dialog asking for confirmation from the user
   * if they want to cancel the creation of the story, and returns the
   * GUI to the game setup GUI if the user confirms.
   */
  private void cancelCreationOfStory() {
    Alert confirmCancelAlert = new Alert(Alert.AlertType.CONFIRMATION);
    confirmCancelAlert.setTitle("Cancel creation of story");
    confirmCancelAlert.setContentText("Are you sure you want to cancel the creation of the story?");
    Optional<ButtonType> result = confirmCancelAlert.showAndWait();

    if (result.isPresent() && result.get() == ButtonType.OK) {
      GameSetupController gameSetupController = new GameSetupController(stage);
      gameSetupController.showContent();
    }
  }
}

package no.ntnu.idat2001.mappe19.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import no.ntnu.idat2001.mappe19.io.StoryFileReader;
import no.ntnu.idat2001.mappe19.model.Game;
import no.ntnu.idat2001.mappe19.model.Player;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.goals.*;
import no.ntnu.idat2001.mappe19.view.GameSetupView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Controller class for the GameSetupView class.
 * Handles displaying and updating the view, and contains the methods triggered by the buttons.
 */
public class GameSetupController {
  private final Stage stage;
  private final GameSetupView view;
  private final ArrayList<Goal> goals;

  /**
   * Constructs a GameSetupController object with a Stage object
   * used to display the GameSetupView created by an object of this class.
   *
   * @param stage the Stage object used to display the view.
   */
  public GameSetupController(Stage stage) {
    this.stage = stage;
    this.view = new GameSetupView();
    this.goals = new ArrayList<>();

    setupButtons();
  }

  /**
   * Displays the GUI elements controlled by objects of the GameSetupController class.
   */
  public void showContent() {
    stage.setScene(new Scene(view, 500, 500));
    stage.show();
  }

  /**
   * Binds methods to the buttons created by the GameSetupView object in a GameSetupController object.
   */
  private void setupButtons() {
    view.getAddGoalBtn().setOnAction(actionEvent -> addGoal());
    view.getSeeGoalsBtn().setOnAction(actionEvent -> showSeeGoalsAlert());
    view.getLoadStoryBtn().setOnAction(actionEvent -> startGame());
    view.getCreateStoryBtn().setOnAction(actionEvent -> goToCreateStoryView());
  }

  /**
   * Shows the dialogs for adding a goal to a game, and registers the new goal
   * if the process is completed.
   */
  private void addGoal() {
    final String HEALTH_GOAL_CHOICE_STRING = "Health Goal";
    final String GOLD_GOAL_CHOICE_STRING = "Gold Goal";
    final String INVENTORY_GOAL_CHOICE_STRING = "Inventory Goal";
    final String SCORE_GOAL_CHOICE_STRING = "Score Goal";

    ArrayList<String> choices = new ArrayList<>();
    choices.add(HEALTH_GOAL_CHOICE_STRING);
    choices.add(GOLD_GOAL_CHOICE_STRING);
    choices.add(SCORE_GOAL_CHOICE_STRING);
    choices.add(INVENTORY_GOAL_CHOICE_STRING);

    ChoiceDialog<String> goalTypeDialog = new ChoiceDialog<>(HEALTH_GOAL_CHOICE_STRING, choices);
    goalTypeDialog.setTitle("Goal Dialog");
    goalTypeDialog.setHeaderText("Choose the type of goal to add");
    goalTypeDialog.setContentText("Goal type:");

    Optional<String> typeResult = goalTypeDialog.showAndWait();
    if (typeResult.isPresent()) {
      Goal goalToAdd = null;

      switch (typeResult.get()) {
        case (HEALTH_GOAL_CHOICE_STRING) -> {
          Optional<String> amountResult = showGoalAmountInputDialog("Health Goal",
              "Enter health goal amount:");

          if (amountResult.isPresent()) {
            goalToAdd = new HealthGoal(Integer.parseInt(amountResult.get()));
          }
        }
        case (GOLD_GOAL_CHOICE_STRING) -> {
          Optional<String> amountResult = showGoalAmountInputDialog("Gold Goal",
              "Enter gold goal amount:");

          if (amountResult.isPresent()) {
            goalToAdd = new GoldGoal(Integer.parseInt(amountResult.get()));
          }
        }
        case (SCORE_GOAL_CHOICE_STRING) -> {
          Optional<String> amountResult = showGoalAmountInputDialog("Score Goal",
              "Enter score goal amount:");

          if (amountResult.isPresent()) {
            goalToAdd = new ScoreGoal(Integer.parseInt(amountResult.get()));
          }
        }
        case ("Inventory Goal") -> goalToAdd = createInventoryGoal();
        default -> {
          Alert errorParsingGoalTypeAlert = new Alert(Alert.AlertType.ERROR);
          errorParsingGoalTypeAlert.setTitle("Goal dialog");
          errorParsingGoalTypeAlert.setHeaderText("Error parsing goal type.");
          errorParsingGoalTypeAlert.setContentText("An error occurred while parsing the selected goal type.\n"
              + "The process will be aborted.");
          errorParsingGoalTypeAlert.showAndWait();
        }
      }

      if (goalToAdd != null) {
        goals.add(goalToAdd);
      } else {
        Alert noItemsToAddAlert = new Alert(Alert.AlertType.INFORMATION);
        noItemsToAddAlert.setContentText("No goal was added.");
      }
    }
  }

  /**
   * Shows a dialog for inputting an integer amount for a specified type if goal.
   *
   * @param headerText the header text for the dialog describing what type of goal to input the amount for.
   * @param contextText the context text of the dialog describing what to input.
   * @return the goal amount inputted in the dialog as a String Optional.
   */
  private Optional<String> showGoalAmountInputDialog(String headerText, String contextText) {
    TextInputDialog goalContentDialog = new TextInputDialog();
    goalContentDialog.setTitle("Goal Dialog");
    goalContentDialog.setHeaderText(headerText);
    goalContentDialog.setContentText(contextText);
    TextField goalContentDialogTextField = goalContentDialog.getEditor();
    BooleanBinding isInvalid = Bindings.createBooleanBinding(
        () -> !isValidAmount(goalContentDialogTextField.getText()),
        goalContentDialogTextField.textProperty());
    goalContentDialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(isInvalid);
    return goalContentDialog.showAndWait();
  }

  /**
   * Shows dialogs specifying the contents of an InventoryGoal.
   *
   * @return an InventoryGoal object representing the content specified on the dialogs.
   */
  private InventoryGoal createInventoryGoal() {
    boolean addingItemsToGoal = true;
    ArrayList<String> items = new ArrayList<>();

    while (addingItemsToGoal) {
      TextInputDialog goalContentDialog = new TextInputDialog();
      goalContentDialog.setTitle("Goal Dialog");
      goalContentDialog.setHeaderText("Inventory Goal");
      goalContentDialog.setContentText("Enter name of item to add to goal:");
      TextField goalContentDialogTextField = goalContentDialog.getEditor();
      BooleanBinding isInvalid = Bindings.createBooleanBinding(
          () -> goalContentDialogTextField.getText().equals(""),
          goalContentDialogTextField.textProperty());
      goalContentDialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(isInvalid);
      Optional<String> itemResult = goalContentDialog.showAndWait();

      if (itemResult.isPresent()) {
        items.add(goalContentDialogTextField.getText());
        Alert addMoreItemsAlert = new Alert(Alert.AlertType.CONFIRMATION);
        addMoreItemsAlert.setTitle("Goal Dialog");
        addMoreItemsAlert.setContentText("Do you want to add more items to the inventory goal?");
        Optional<ButtonType> addMoreItemsResult = addMoreItemsAlert.showAndWait();
        if (addMoreItemsResult.isEmpty() || addMoreItemsResult.get() != ButtonType.OK) {
          addingItemsToGoal = false;
        }
      } else {
        addingItemsToGoal = false;
      }
    }

    if (items.size() > 0) {
      return new InventoryGoal(items);
    } else {
      Alert noItemsToAddAlert = new Alert(Alert.AlertType.INFORMATION);
      noItemsToAddAlert.setContentText("No goal will be added as there were no items specified.");
      return null;
    }
  }

  /**
   * Checks if a String used to specify an amount for
   * a GoldGoal, HealthGoal or ScoreGoal is valid.
   * Used in the input dialogs for disabling or enabling the confirm button,
   * to prevent illegal values.
   *
   * @param s the String to check.
   * @return true if the String contains a valid amount, false if not.
   */
  private boolean isValidAmount(String s) {
    if (s == null) {
      return false;
    }
    try {
      int i = Integer.parseInt(s);
      if (i < 0) {
        return false;
      }
    } catch (NumberFormatException e) {
      return false;
    }
    return true;
  }

  /**
   * Shows the user a window listing all currently registered goals.
   */
  private void showSeeGoalsAlert() {
    String message = "There are no goals registered.";

    if (goals.size() > 0) {
      StringBuilder goalsMessage = new StringBuilder();
      goals.forEach((goal) -> goalsMessage.append(goal).append("\n"));
      message = goalsMessage.toString();
    }

    Alert goalsAlert = new Alert(Alert.AlertType.INFORMATION);
    goalsAlert.setTitle("Goals");
    goalsAlert.setHeaderText("Currently registered goals.");
    goalsAlert.setContentText(message);
    goalsAlert.showAndWait();
  }

  /**
   * Shows the confirmation dialog for confirming
   * if the player wants to start the game or not,
   * and starts the game if confirmed by the user.
   */
  private void startGame() {
    try {
      Story story = StoryFileReader.readFromFile(view.getPathToPathsFileFieldValue());
      Player player = new Player.PlayerBuilder(view.getPlayerNameFieldValue())
          .setHealth(view.getPlayerHealthFieldValue())
          .setScore(0)
          .setGold(view.getPlayerGoldFieldValue())
          .build();
      Game game = new Game(player, story, goals);

      Alert startGameAlert = new Alert(Alert.AlertType.CONFIRMATION);
      startGameAlert.setTitle("Start game");
      startGameAlert.setHeaderText("Do you want to start the specified game?");
      String contextText = "The story with the title " + story.getTitle() + "\n"
          + "will be played by the player by the name of " + player.getName() + ".\n"
          + "This player will start with " + player.getHealth() + " health and "
          + player.getGold() + " gold.";
      int amountOfBrokenLinks = story.getBrokenLinks().size();
      if (amountOfBrokenLinks > 0) {
        contextText += "\n\nThe selected story has " + amountOfBrokenLinks + "broken links that will not "
            + "show during the game.";
      }
      startGameAlert.setContentText(contextText);
      Optional<ButtonType> result = startGameAlert.showAndWait();
      if (result.isPresent() && result.get() == ButtonType.OK) {
        GameController gameController = new GameController(game, stage);
        gameController.showContent();
      }
    } catch (IOException e) {
      Alert storyAlert = new Alert(Alert.AlertType.ERROR);
      storyAlert.setTitle("Error loading paths file.");
      storyAlert.setContentText(e.getMessage());
      storyAlert.showAndWait();
    } catch (IllegalArgumentException e) {
      Alert playerAlert = new Alert(Alert.AlertType.ERROR);
      playerAlert.setTitle("Error creating player or game object.");
      playerAlert.setContentText(e.getMessage());
      playerAlert.showAndWait();
    }
  }

  /**
   * Changes the displayed GUI to the GUI for creating a story.
   */
  private void goToCreateStoryView() {
    StoryCreationController storyCreationController = new StoryCreationController(stage);
    storyCreationController.showContent();
  }
}

package no.ntnu.idat2001.mappe19.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * View class that creates the GUI elements showed when setting up a game.
 * This is the first window showed when starting the application.
 */
public class GameSetupView extends BorderPane {
  private TextField pathToPathsFileField;
  private TextField playerNameField;
  private TextField playerHealthField;
  private TextField playerGoldField;
  private Button loadStoryBtn;
  private Button addGoalBtn;
  private Button seeGoalsBtn;
  private Button createStoryBtn;

  /**
   * Constructs a GameSetupView object.
   */
  public GameSetupView() {
    this.setCenter(createCenterContainer());
    this.setBottom(createBottomContainer());
  }

  /**
   * Returns the button object representing the "Start game" button.
   *
   * @return the Button object.
   */
  public Button getLoadStoryBtn() {
    return loadStoryBtn;
  }

  /**
   * Returns the button object representing the "Add new goal" button.
   *
   * @return the Button object.
   */
  public Button getAddGoalBtn() {
    return addGoalBtn;
  }

  /**
   * Returns the button object representing the "See goals" button.
   *
   * @return the Button object.
   */
  public Button getSeeGoalsBtn() {
    return seeGoalsBtn;
  }

  /**
   * Returns the button object representing the "Create story" button.
   *
   * @return the Button object.
   */
  public Button getCreateStoryBtn() {
    return createStoryBtn;
  }

  /**
   * Returns the current String written in the
   * text input filed for specifying the path to a Story file.
   *
   * @return the String in the TextField.
   */
  public String getPathToPathsFileFieldValue() {
    return pathToPathsFileField.getText();
  }

  /**
   * Returns the current String written in the
   * text input filed for specifying the name of the player.
   *
   * @return the String in the TextField.
   */
  public String getPlayerNameFieldValue() {
    return playerNameField.getText();
  }

  /**
   * Returns the current value written
   * in the text input field for specifying the initial health of the player.
   *
   * @return the value as an integer, -1 if the value is not an integer.
   */
  public int getPlayerHealthFieldValue() {
    try {
      return Integer.parseInt(playerHealthField.getText());
    } catch (NumberFormatException e) {
      return -1;
    }
  }

  /**
   * Returns the current value written
   * in the text input field for specifying the initial gold of the player.
   *
   * @return the value as an integer, -1 if the value is not an integer.
   */
  public int getPlayerGoldFieldValue() {
    try {
      return Integer.parseInt(playerGoldField.getText());
    } catch (NumberFormatException e) {
      return -1;
    }
  }

  /**
   * Creates the VBox used as the container for the GUI elements
   * in the center of the GameSetupView.
   *
   * @return the VBox containing the GUI objects.
   */
  private VBox createCenterContainer() {
    VBox container = new VBox();
    container.setPadding(new Insets(10));
    container.setSpacing(10);

    //Sets up GUI for specifying paths file
    pathToPathsFileField = new TextField();
    Label pathToPathsFileFieldLabel = new Label("Path to story (.paths file): ");
    pathToPathsFileFieldLabel.setLabelFor(pathToPathsFileField);
    HBox pathsFileBox = new HBox(pathToPathsFileFieldLabel, pathToPathsFileField);
    pathsFileBox.setSpacing(10);
    pathsFileBox.setPadding(new Insets(10));

    //Sets up GUI for inputting Player information
    playerNameField = new TextField();
    playerHealthField = new TextField();
    playerGoldField = new TextField();

    HBox playerNameBox = new PlayerInputHBox(playerNameField, "Player name:");
    HBox playerHealthBox = new PlayerInputHBox(playerHealthField, "Initial health (integer, 0 or above):");
    HBox playerGoldBox = new PlayerInputHBox(playerGoldField, "Initial gold (integer, 0 or above):");

    HBox buttonsBox = new HBox();
    buttonsBox.setPadding(new Insets(10));
    buttonsBox.setSpacing(10);
    seeGoalsBtn = new Button("See goals");
    addGoalBtn = new Button("Add new goal");
    loadStoryBtn = new Button("Start game");
    buttonsBox.getChildren().addAll(seeGoalsBtn, addGoalBtn, loadStoryBtn);

    container.getChildren().addAll(pathsFileBox,
        playerNameBox,
        playerHealthBox,
        playerGoldBox,
        buttonsBox);

    return container;
  }

  /**
   * Creates the HBox used as the container for the GUI elements
   * in the bottom of the GameSetupView.
   *
   * @return the HBox containing the GUI objects.
   */
  private HBox createBottomContainer() {
    HBox bottomContainer = new HBox();
    bottomContainer.setSpacing(10);
    bottomContainer.setPadding(new Insets(10));

    createStoryBtn = new Button("Create story");

    bottomContainer.getChildren().add(createStoryBtn);

    return bottomContainer;
  }

  /**
   * Class used for creating uniform HBoxes for the TextField objects used for specifying Player values.
   */
  private static class PlayerInputHBox extends HBox {

    /**
     * Creates a PlayerInputHBox object with the given TextField and text for the TextField label.
     *
     * @param textField the TextField object in the PlayerInputHBox object.
     * @param labelText the label text as a String.
     */
    public PlayerInputHBox(TextField textField, String labelText) {
      this.setPadding(new Insets(10));
      this.setSpacing(10);
      Label label = new Label(labelText);
      label.setLabelFor(textField);
      this.getChildren().add(label);
      this.getChildren().add(textField);
    }
  }
}

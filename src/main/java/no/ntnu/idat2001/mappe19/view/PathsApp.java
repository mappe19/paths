package no.ntnu.idat2001.mappe19.view;


import javafx.application.Application;

import javafx.stage.Stage;
import no.ntnu.idat2001.mappe19.controller.GameSetupController;

/**
 * The main application class.
 * Handles launching the GUI.
 */
public class PathsApp extends Application {

  /**
   * Method called when the application is launched.
   * Handles displaying the first window of the GUI.
   *
   * @param stage the primary Stage object used for displaying the GUI.
   * @throws Exception if something goes wrong during execution of the method.
   */
  @Override
  public void start(Stage stage) throws Exception {
    stage.setResizable(false);
    stage.setTitle("Paths");

    GameSetupController controller = new GameSetupController(stage);
    controller.showContent();
  }

  /**
   * Main method called by the App class to launch the application.
   *
   * @param args the command line arguments passed to the application.
   */
  public static void main(String[] args) {
    launch(args);
  }
}

package no.ntnu.idat2001.mappe19.view;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import no.ntnu.idat2001.mappe19.model.Passage;

import java.util.List;

/**
 * View class that creates the GUI elements showed when creating a story.
 */
public class StoryCreationView extends BorderPane {
  private TextField filePathField;
  private TextField storyTitleField;
  private Text storyContent;
  private Button saveStoryBtn;
  private Button addPassageBtn;
  private Button addLinkBtn;
  private Button addActionBtn;
  private Button cancelButton;

  /**
   * Constructs a StoryCreationView object.
   */
  public StoryCreationView() {
    this.setTop(createTopContainer());
    this.setCenter(createCenterContainer());
    this.setBottom(createBottomContainer());
  }

  /**
   * Returns the String in the text field
   * for specifying the path to the file where
   * the Story will be saved.
   *
   * @return the file path String.
   */
  public String getFilePathFieldText() {
    return filePathField.getText();
  }

  /**
   * Returns the String in the text field
   * for specifying the title of the Story.
   *
   * @return the story title String.
   */
  public String getStoryTitleFieldText() {
    return storyTitleField.getText();
  }

  /**
   * Returns the button object representing the "Save story" button.
   *
   * @return the Button object.
   */
  public Button getSaveStoryBtn() {
    return saveStoryBtn;
  }

  /**
   * Returns the button object representing the "Add passage" button.
   *
   * @return the Button object.
   */
  public Button getAddPassageBtn() {
    return addPassageBtn;
  }

  /**
   * Returns the button object representing the "Add link" button.
   *
   * @return the Button object.
   */
  public Button getAddLinkBtn() {
    return addLinkBtn;
  }

  /**
   * Returns the button object representing the "Add action" button.
   *
   * @return the Button object.
   */
  public Button getAddActionBtn() {
    return addActionBtn;
  }

  /**
   * Returns the button object representing the "Cancel" button.
   *
   * @return the Button object.
   */
  public Button getCancelButton() {
    return cancelButton;
  }

  /**
   * Method for updating the text displaying the current content of a Story that is
   * being created.
   *
   * @param passages a list of the passages in the Story, the first of which is the opening passage.
   */
  public void updateStoryContent(List<Passage> passages) {
    StringBuilder text = new StringBuilder("Current content of the story:\n\n");

    if (!passages.isEmpty()) {
      Passage openingPassage = passages.get(0);
      text.append("Name of opening passage: ").append(openingPassage.getTitle()).append("\n");
      text.append("Content of opening passage: ").append(openingPassage.getContent()).append("\n");

      if (openingPassage.hasLinks()) {
        text.append(createLinksAndActionsStringForPassage(openingPassage));
      }
      text.append("\n");

      if (passages.size() > 1) {
        for (int i = 1; i < passages.size(); i++) {
          Passage passage = passages.get(i);
          text.append("Name of passage: ").append(passage.getTitle()).append("\n");
          text.append("Content of passage: ").append(passage.getContent()).append("\n");

          if (passage.hasLinks()) {
            text.append(createLinksAndActionsStringForPassage(passage));
          }
          text.append("\n");
        }
      }
    } else {
      text.append("No content.");
    }

    storyContent.setText(text.toString());
  }

  /**
   * Helper method for creating a String describing the links and
   * actions for a specific Passage object.
   *
   * @param passage the Passage object to create the String for.
   * @return the String describing the links and actions.
   */
  private String createLinksAndActionsStringForPassage(Passage passage) {
    StringBuilder linksAndActionsText = new StringBuilder();

    linksAndActionsText.append("Links of passage:\n");
    passage.getLinks().forEach(link -> {
      linksAndActionsText.append("Text of link: ").append(link.getText()).append("\n");
      linksAndActionsText.append("Link reference: ").append(link.getReference()).append("\n");

      if (!link.getActions().isEmpty()) {
        linksAndActionsText.append("Actions of link:\n");
        link.getActions().forEach(action -> linksAndActionsText.append(action).append("\n"));
      }
    });
    return linksAndActionsText.toString();
  }

  /**
   * Creates the VBox used as the container for the GUI elements
   * in the top part of the StoryCreationView.
   * Contains the elements for specifying the file path to
   * where the Story object will be saved and for specifying the title of the Story.
   *
   * @return the VBox containing the GUI objects.
   */
  private VBox createTopContainer() {
    VBox topContainer = new VBox();
    topContainer.setSpacing(10);
    topContainer.setPadding(new Insets(10));

    HBox filePathBox = new HBox();
    filePathBox.setSpacing(10);
    Label filePathLabel = new Label("Path to .paths file: ");
    filePathField = new TextField();
    filePathBox.getChildren().add(filePathLabel);
    filePathBox.getChildren().add(filePathField);

    HBox storyTitleBox = new HBox();
    storyTitleBox.setSpacing(10);
    Label storyTitleLabel = new Label("Title of story:");
    storyTitleField = new TextField();
    storyTitleBox.getChildren().add(storyTitleLabel);
    storyTitleBox.getChildren().add(storyTitleField);

    topContainer.getChildren().add(filePathBox);
    topContainer.getChildren().add(storyTitleBox);

    return topContainer;
  }

  /**
   * Creates the ScrollPane used as the container for the GUI elements
   * in the center part of the StoryCreationView.
   * Contains a Text object used to display the currently added content
   * in the Story that is being created.
   *
   * @return the ScrollPane containing the GUI objects.
   */
  private ScrollPane createCenterContainer() {
    ScrollPane centerContainer = new ScrollPane();
    centerContainer.setPadding(new Insets(10));
    centerContainer.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    centerContainer.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    storyContent = new Text();
    centerContainer.setContent(storyContent);

    return centerContainer;
  }

  /**
   * Creates the HBox used as the container for the GUI elements
   * in the bottom part of the StoryCreationView.
   * Contains the buttons for saving the story, adding a passage, link or action and for canceling
   * the creation of the story.
   *
   * @return the HBox containing the GUI objects.
   */
  private HBox createBottomContainer() {
    HBox bottomContainer = new HBox();
    bottomContainer.setPadding(new Insets(10));
    bottomContainer.setSpacing(10);

    saveStoryBtn = new Button("Save story");
    addPassageBtn = new Button("Add passage");
    addLinkBtn = new Button("Add link");
    addActionBtn = new Button("Add action");
    cancelButton = new Button("Cancel");

    bottomContainer.getChildren().addAll(saveStoryBtn, addPassageBtn, addLinkBtn, addActionBtn, cancelButton);

    return bottomContainer;
  }
}

package no.ntnu.idat2001.mappe19.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Player;

import java.util.List;

/**
 * View class that creates the GUI elements showed when playing a game.
 */
public class GameView extends BorderPane {
  private Label playerName;
  private Label playerHealth;
  private Label playerScore;
  private Label playerGold;
  private Label passageTitle;
  private Label passageContent;
  private Button seeInventoryBtn;
  private Button seeGoalsStatusBtn;
  private Button endGameButton;
  private Button restartGameButton;
  private Button helpButton;
  private FlowPane linkButtonsContainer;

  /**
   * Constructs a GameView object.
   */
  public GameView() {
    this.setTop(createTopContainer());
    this.setCenter(creteCenterContainer());
    this.setBottom(createBottomContainer());
  }

  /**
   * Returns the button object representing the "See status of goals" button.
   *
   * @return the Button object.
   */
  public Button getSeeGoalsStatusBtn() {
    return seeGoalsStatusBtn;
  }

  /**
   * Returns the button object representing the "See inventory" button.
   *
   * @return the Button object.
   */
  public Button getSeeInventoryBtn() {
    return seeInventoryBtn;
  }

  /**
   * Returns the button object representing the "End game" button.
   *
   * @return the Button object.
   */
  public Button getEndGameButton() {
    return endGameButton;
  }

  /**
   * Returns the button object representing the "Restart game" button.
   *
   * @return the Button object.
   */
  public Button getRestartGameButton() {
    return restartGameButton;
  }

  /**
   * Returns the button object representing the "Help" button.
   *
   * @return the Button object.
   */
  public Button getHelpButton() {
    return helpButton;
  }

  /**
   * Method used for updating the content displayed by the view.
   * The elements that are updated are the player status, passage title, passage content
   * and the buttons representing the links contained in the passage.
   *
   * @param player the Player object containing the player's information.
   * @param passage the Passage object representing the current Passage.
   * @param linkButtons the Button objects representing the Passage object's links contained in a List.
   */
  public void update(Player player, Passage passage, List<Button> linkButtons) {
    playerName.setText(player.getName());
    playerHealth.setText("" + player.getHealth());
    playerScore.setText("" + player.getScore());
    playerGold.setText("" + player.getGold());
    passageTitle.setText(passage.getTitle());
    passageContent.setText(passage.getContent());
    linkButtonsContainer.getChildren().removeAll(linkButtonsContainer.getChildren());
    if (linkButtons.size() > 0) {
      linkButtonsContainer.getChildren().addAll(linkButtons);
    } else {
      linkButtonsContainer.getChildren().add(new Label("There is nothing to do."));
      endGameButton.setVisible(true);
    }
  }

  /**
   * Creates the HBox used as the container for the GUI elements
   * in the top part of the GameView.
   * This HBox contains the elements for displaying Player status
   * and the buttons for viewing goals and inventory.
   *
   * @return the HBox containing the GUI objects.
   */
  private HBox createTopContainer() {
    HBox topContainer = new HBox();
    topContainer.setSpacing(10);
    topContainer.setPadding(new Insets(10));

    Label playerNameText = new Label("Player: ");
    playerName = new Label();
    Label playerHealthText = new Label("Health: ");
    playerHealth = new Label();
    Label playerGoldText = new Label("Gold: ");
    playerGold = new Label();
    Label playerScoreText = new Label("Score: ");
    playerScore = new Label();

    seeInventoryBtn = new Button("See inventory");
    seeGoalsStatusBtn = new Button("See status of goals");

    topContainer.getChildren().add(playerNameText);
    topContainer.getChildren().add(playerName);
    topContainer.getChildren().add(playerHealthText);
    topContainer.getChildren().add(playerHealth);
    topContainer.getChildren().add(playerGoldText);
    topContainer.getChildren().add(playerGold);
    topContainer.getChildren().add(playerScoreText);
    topContainer.getChildren().add(playerScore);
    topContainer.getChildren().add(seeInventoryBtn);
    topContainer.getChildren().add(seeGoalsStatusBtn);

    return topContainer;
  }

  /**
   * Creates the VBox used as the container for the GUI elements
   * in the center of the GameView.
   * Contains the passage title, content and link buttons.
   *
   * @return the VBox containing the GUI objects.
   */
  private VBox creteCenterContainer() {
    VBox centerContainer = new VBox();
    centerContainer.setPadding(new Insets(10));
    centerContainer.setSpacing(10);

    passageTitle = new Label();
    passageTitle.setFont(new Font(20));
    HBox passageTitleBox = new HBox(passageTitle);

    passageContent = new Label();
    HBox passageContentBox = new HBox(passageContent);

    linkButtonsContainer = new FlowPane();
    linkButtonsContainer.setHgap(10);
    linkButtonsContainer.setVgap(10);
    HBox linkButtonsBox = new HBox(linkButtonsContainer);

    centerContainer.getChildren().add(passageTitleBox);
    centerContainer.getChildren().add(passageContentBox);
    centerContainer.getChildren().add(linkButtonsBox);

    return centerContainer;
  }

  /**
   * Creates the HBox used as the container for the GUI elements
   * in the bottom of the GameView.
   * Contains the buttons for viewing help screen, restarting the game and ending a game.
   *
   * @return the HBox containing the GUI objects.
   */
  private HBox createBottomContainer() {
    HBox bottomContainer = new HBox();
    bottomContainer.setSpacing(10);
    bottomContainer.setPadding(new Insets(10));

    endGameButton = new Button("End game");
    endGameButton.setVisible(false);

    restartGameButton = new Button("Restart game");
    helpButton = new Button("Help");

    bottomContainer.getChildren().add(helpButton);
    bottomContainer.getChildren().add(restartGameButton);
    bottomContainer.getChildren().add(endGameButton);

    return bottomContainer;
  }
}

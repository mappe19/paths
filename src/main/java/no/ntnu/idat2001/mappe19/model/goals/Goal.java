package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Interface representing a goal that can be achieved.
 */
public interface Goal {
  /**
   * Tells you if the goal ha been achieved.
   *
   * @param player reference to the Player object the goal applies to.
   * @return a boolean value which is true if the goal is achieved and false if not.
   */
  public boolean isFulfilled(Player player);
}

package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * A goal for the player to achieve a minimum score.
 */
public class ScoreGoal implements Goal{
  private final Integer minimumPoints;

  /**
   * Constructs a goal with a given amount of points.
   *
   * @param minimumPoints the given amount of points as an integer.
   * @throws IllegalArgumentException if the specified amount of points is less than zero.
   */
  public ScoreGoal(Integer minimumPoints) {
    if (minimumPoints < 0) {
      throw new IllegalArgumentException("Minimum required score can not be less than zero.");
    }
    this.minimumPoints = minimumPoints;
  }

  /**
   * Tells you if the minimum score has been achieved.
   *
   * @param player reference to the Player object the goal applies to.
   * @return boolean if the minimum score or more has been reached, and false if not.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getScore() >= minimumPoints;
  }

  /**
   * Returns a text description of the object.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "Score goal, minimum " + minimumPoints + " points.";
  }
}

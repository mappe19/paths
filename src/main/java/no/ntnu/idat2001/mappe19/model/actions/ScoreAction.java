package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Class used for changing the players score value
 */
public class ScoreAction implements Action{
  private final int points;

  /**
   * Sets the value of the points to be added.
   *
   * @param points the amount of points.
   */
  public ScoreAction(int points) {
    this.points = points;
  }

  /**
   * {@inheritDoc}
   * Adds points to the player score.
   * Sets the score to zero if there is an attempt to remove more points than the player has.
   *
   * @throws IllegalArgumentException if the given Player object is null.
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player object cannot be null.");
    }
    try {
      player.addScore(points);
    } catch (IllegalArgumentException e) {
      player.addScore(player.getScore() * -1);
    }
  }

  /**
   * Gives a text description of the score action.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "- score_action, value: " + points;
  }
}

package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Class used for changing the player characters health value.
 */
public class HealthAction implements Action{
  private final int health;

  /**
   * Sets the value of the health to be added.
   *
   * @param health the amount of health.
   */
  public HealthAction(int health) {
    this.health = health;
  }

  /**
   *{@inheritDoc}
   * Adds health to the player.
   * Sets the health to zero if there is an attempt to remove more health than the player has.
   *
   * @throws IllegalArgumentException if the given Player object is null.
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player object cannot be null.");
    }
    try {
      player.addHealth(health);
    } catch (IllegalArgumentException e) {
      player.addHealth(player.getHealth() * -1);
    }
  }

  /**
   * Gives a text description of the health action.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "- health_action, value: " + health;
  }
}

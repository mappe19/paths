package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Interface used to implement action classes.
 */
public interface Action {
  /**
   * Executes an action on a player character.
   *
   * @param player the Player object the Action is executed on.
   */
  void execute(Player player);
}

package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * A goal for the player to achieve a minimum amount of health.
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;

  /**
   * Constructs a goal with a given amount of health.
   *
   * @param minimumHealth the minimum amount of health to be achieved as an integer.
   * @throws IllegalArgumentException if the specified amount of health is less than zero.
   */
  public HealthGoal(int minimumHealth) {
    if (minimumHealth < 0) {
      throw new IllegalArgumentException("Minimum required health can not be less than zero.");
    }
    this.minimumHealth = minimumHealth;
  }

  /**
   * Tells you if the minimum amount of health has been achieved.
   *
   * @param player reference to the Player object the goal applies to.
   * @return boolean if the minimum health amount or more has been reached, and false if not.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getHealth() >= minimumHealth;
  }

  /**
   * Returns a text description of the object.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "Health goal, minimum " + minimumHealth + " health.";
  }
}

package no.ntnu.idat2001.mappe19.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class used for creating passage objects.
 */
public class Passage {
  private final String title;

  private final String content;

  private final List<Link> links;

  /**
   * Constructor used to create passage objects.
   *
   * @param title Title of the passage, also works as identificator.
   * @param content Text representing a paragraph or part of a dialog.
   * The constructor also initializes a list for links to connecting passages.
   */
  public Passage(String title, String content) {
    this.title = title;
    this.content = content;
    links = new ArrayList<>();
  }

  /**
   * Method used for getting the passage title.
   *
   * @return returns the passage title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Method used for getting the passage content.
   *
   * @return returns the passage content.
   */
  public String getContent() {
    return content;
  }

  /**
   * Method used for adding a link to the links list.
   *
   * @param link link to a neighboring passage.
   * @return returns a boolean value depending on if the link was successfully added.
   * @throws IllegalArgumentException if the link parameter is null.
   */
  public boolean addLink(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("The link parameter can not be null.");
    }
    for (Link linkInList : links) {
      if (link.equals(linkInList)) {
        return false;
      }
    }
    return links.add(link);
  }

  public List<Link> getLinks() {
    return links;
  }

  /**
   * Method tests if there are any links to neighboring passages.
   *
   * @return returns true if there are neighboring passages,
   * returns false if there are no neighboring passages.
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * toString method used to represent the passage object in the form of a string.
   *
   * @return string representing the passage.
   */
  @Override
  public String toString() {
    StringBuilder linkString = new StringBuilder();
    for (Link link : links) {
      linkString.append(link).append("\n");
    }
    return "Passage: " + title +
        "\nContent: " + content +
        "\nLinks:\n" + linkString;
  }

  /**
   * Tests if another object is the same as this object.
   *
   * @param o takes in an object for testing.
   *          Checks if the object type is the same as this object.
   *          Checks if the parameters within are the same as this object.
   * @return returns true if this object and the tested object is identical.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Passage passage = (Passage) o;
    return Objects.equals(title, passage.title) && Objects.equals(content, passage.content);
  }

  /**
   * Creates a hashCode for the object.
   *
   * @return returns the hash as integer.
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }
}

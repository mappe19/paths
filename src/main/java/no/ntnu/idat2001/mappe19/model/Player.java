package no.ntnu.idat2001.mappe19.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to create the player object.
 * The player plays the game through the player object.
 */
public class Player {
  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  /**
   * Constructor that builds a Player object from a PlayerBuilder object.
   *
   * @param builder the PlayerBuilder object used to build a Player object.
   */
  private Player(PlayerBuilder builder) {
    this.name = builder.name;
    this.health = builder.health;
    this.score = builder.score;
    this.gold = builder.gold;
    inventory = new ArrayList<>();
  }

  /**
   * Getter method, returns player name.
   *
   * @return player name.
   */
  public String getName() {
    return name;
  }

  /**
   * Method used for changing the players health value.
   *
   * @param health value to be added to health.
   * @throws IllegalArgumentException throws an exception if the resulting player health is less than 0.
   */
  public void addHealth(int health) throws IllegalArgumentException {
    if (this.health + health < 0) {
      throw new IllegalArgumentException("Health cannot be less than zero.");
    }
    this.health += health;
  }

  /**
   * Getter method, returns health value.
   *
   * @return health value.
   */
  public int getHealth() {
    return health;
  }

  /**
   * Adds points to player score.
   *
   * @param points points to be added.
   * @throws IllegalArgumentException if score is set to less than zero.
   */
  public void addScore(int points) throws IllegalArgumentException {
    if (this.score + points < 0) {
      throw new IllegalArgumentException("Score cannot be less than 0");
    }
    this.score += points;
  }

  /**
   * Getter method, returns score.
   *
   * @return score value.
   */
  public int getScore() {
    return score;
  }

  /**
   * Adds gold to the player total.
   *
   * @param gold gold to be added.
   * @throws IllegalArgumentException if gold is set to less than zero.
   */
  public void addGold(int gold) throws IllegalArgumentException {
    if (this.gold + gold < 0) {
      throw new IllegalArgumentException("Gold cannot be less tha 0");
    }
    this.gold += gold;
  }

  /**
   * Getter method, returns gold.
   *
   * @return gold value.
   */
  public int getGold() {
    return gold;
  }

  /**
   * Adds an item represented by a string to the player inventory.
   *
   * @param item Item represented as string.
   * @throws IllegalArgumentException if an empty String is added to inventory.
   */
  public void addToInventory(String item) {
    if (item.equals("")) {
      throw new IllegalArgumentException("Cannot add empty String to inventory.");
    }
    inventory.add(item);
  }

  /**
   * Getter method, returns inventory ArrayList.
   *
   * @return Inventory as ArrayList.
   */
  public List<String> getInventory() {
    return new ArrayList<>(inventory);
  }

  /**
   * Builder design pattern implementation of the Player class.
   * Makes it possible to only include specific attributes in a Player object
   * instead of specifying all of them.
   * Unspecified integer attributes of the Player object defaults to zero.
   */
  public static class PlayerBuilder {
    private final String name;
    private int health;
    private int score;
    private int gold;

    /**
     * Constructor that creates the PlayerBuilder object used in building a Player object.
     * The name of a player is obligatory and must be specified in the constructor of the Builder.
     *
     * @param name the name of the player as a String.
     * @throws IllegalArgumentException if the name String is empty.
     */
    public PlayerBuilder(String name) throws IllegalArgumentException {
      if (name.equals("")) {
        throw new IllegalArgumentException("The player can not have an empty String as a name.");
      }
      this.name = name;
    }

    /**
     * Sets the health attribute of the PlayerBuilder object.
     * Makes it so that the built Player object has initial health.
     *
     * @param health the health of the player as an integer higher than zero.
     * @return this PlayerBuilder object
     * @throws IllegalArgumentException if the specified health amount is less than zero
     */
    public PlayerBuilder setHealth(int health) throws IllegalArgumentException {
      if (health < 0) {
        throw new IllegalArgumentException("Health cannot be less than zero.");
      }
      this.health = health;
      return this;
    }

    /**
     * Sets the score attribute of the PlayerBuilder object.
     * Makes it so that the built Player object has an initial score.
     *
     * @param score the initial score of the player as an integer higher than zero.
     * @return this PlayerBuilder object
     * @throws IllegalArgumentException if the specified score amount is less than zero
     */
    public PlayerBuilder setScore(int score) throws IllegalArgumentException {
      if (score < 0) {
        throw new IllegalArgumentException("Score cannot be less than 0");
      }
      this.score = score;
      return this;
    }

    /**
     * Sets the gold attribute of the PlayerBuilder object.
     * Makes it so that the built Player object has an initial gold amount.
     *
     * @param gold the initial gold amount of the player as an integer higher than zero
     * @return this PlayerBuilder object
     * @throws IllegalArgumentException if the specified gold amount is less than zero.
     */
    public PlayerBuilder setGold(int gold) throws IllegalArgumentException {
      if (gold < 0) {
        throw new IllegalArgumentException("Gold cannot be less tha 0");
      }
      this.gold = gold;
      return this;
    }

    /**
     * Builds the Player object based on the PlayerBuilder object.
     *
     * @return the built Player object.
     */
    public Player build() {
      return new Player(this);
    }
  }
}

package no.ntnu.idat2001.mappe19.model;

import no.ntnu.idat2001.mappe19.model.goals.Goal;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used for creating a game environment.
 */
public class Game {
  private final Player player;
  private final Story story;
  private final List<Goal> gameGoals;

  /**
   * Constructor that creates a Game object with the given Player, Story and Goals objects.
   *
   * @param player the player character to be controlled by the user.
   * @param story the games story with passages and actions to be taken.
   * @param gameGoals a list of goals the player needs to fulfill to clear the game.
   * @throws IllegalArgumentException if any parameter is null.
   */
  public Game(Player player, Story story, List<Goal> gameGoals) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player parameter can not be null.");
    }
    this.player = player;
    if (story == null) {
      throw new IllegalArgumentException("The story parameter can not be null.");
    }
    this.story = story;
    if (gameGoals == null) {
      throw new IllegalArgumentException("The gameGoals parameter can not be null.");
    }
    this.gameGoals = new ArrayList<>(gameGoals);
  }

  /**
   * Method used for getting the player object.
   *
   * @return returns the player character.
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Method used for getting the story object.
   *
   * @return returns the game story.
   */
  public Story getStory() {
    return story;
  }

  /**
   * Method used for getting the list of goals.
   *
   * @return returns the list of goals.
   */
  public List<Goal> getGameGoals() {
    return gameGoals;
  }

  /**
   * Method used for selecting the games starting passage.
   *
   * @return returns the opening passage as set in the story object.
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * Method used for moving between passages.
   *
   * @param link Takes in a link corresponding to a neighboring passage.
   * @return returns the passage corresponding to the input link.
   * @throws IllegalArgumentException if the link parameter is null.
   */
  public Passage go(Link link) throws IllegalArgumentException {
    if (link == null) {
      throw new IllegalArgumentException("The link parameter can not be null.");
    }
    return story.getPassage(link);
  }
}

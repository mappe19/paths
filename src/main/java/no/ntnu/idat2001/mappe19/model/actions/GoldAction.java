package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Class used for changing the player characters gold value.
 */
public class GoldAction implements Action {
  private final int gold;

  /**
   * Method sets value of gold to be added.
   *
   * @param gold the amount of gold.
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }

  /**
   * {@inheritDoc}
   * Adds gold to the player.
   * Sets the gold amount to zero if there is an attempt to remove more gold than the player has.
   *
   * @throws IllegalArgumentException if the given Player object is null.
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player object cannot be null.");
    }
    try {
      player.addGold(gold);
    } catch (IllegalArgumentException e) {
      player.addGold(player.getGold() * -1);
    }
  }

  /**
   * Gives a text description of the gold action.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "- gold_action, value: " + gold;
  }
}

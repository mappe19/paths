package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * Class for adding an item to the inventory of the player character.
 */
public class InventoryAction implements Action{
  private final String item;

  /**
   * Sets the item to be added to player inventory.
   *
   * @param item the item to be added.
   */
  public InventoryAction(String item) {
    this.item = item;
  }

  /**
   * {@inheritDoc}
   * Adds an item to the inventory.
   * Does not add the specified item if the String is empty or just whitespace.
   *
   * @throws IllegalArgumentException if the given Player object is null.
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player object cannot be null.");
    }
    if (!item.trim().equals("")) {
      player.addToInventory(item);
    }
  }

  /**
   * Gives a text description of the inventory action.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "- inventory_action, value: " + item;
  }
}

package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;

/**
 * A goal for the player to achieve a minimum amount of gold.
 */
public class GoldGoal implements Goal {
  private final int minimumGold;

  /**
   * Constructs a goal with a given amount of gold.
   *
   * @param minimumGold the minimum amount of gold to be achieved as an integer.
   * @throws IllegalArgumentException if the specified amount of gold is less than zero.
   */
  public GoldGoal(int minimumGold) {
    if (minimumGold < 0) {
      throw new IllegalArgumentException("Minimum required gold can not be less than zero.");
    }
    this.minimumGold = minimumGold;
  }

  /**
   * Tells you if the minimum amount of gold has been achieved.
   *
   * @param player reference to the Player object the goal applies to.
   * @return boolean if the minimum gold amount or more has been reached, and false if not.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getGold() >= minimumGold;
  }

  /**
   * Returns a text description of the object.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    return "Gold goal, minimum " + minimumGold + " gold.";
  }
}

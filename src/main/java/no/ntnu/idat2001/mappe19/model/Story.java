package no.ntnu.idat2001.mappe19.model;

import java.util.*;

/**
 * Represents the story used by the game
 */
public class Story {
  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;

  /**
   * Constructor sets the story title and the opening passage in the game story.
   *
   * @param title Story title.
   * @param openingPassage First passage in the story.
   * @throws IllegalArgumentException if the openingPassage parameter is null.
   */
  public Story(String title, Passage openingPassage) throws IllegalArgumentException {
    this.title = title;
    this.openingPassage = openingPassage;
    if (openingPassage == null) {
      throw new IllegalArgumentException("The openingPassage parameter can not be null.");
    }
    passages = new HashMap<>();
    Link openingPassageLink = new Link(openingPassage.getTitle(), openingPassage.getTitle());
    passages.put(openingPassageLink, openingPassage);
  }

  /**
   * Getter method, returns story title.
   *
   * @return the Story title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Getter method, returns opening passage.
   *
   * @return passage object representing the starting passage.
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * Method adds a passage to the passages hashmap used in the story.
   *
   * @param passage passage object to be added.
   * @throws IllegalArgumentException if the passage parameter is null.
   */
  public boolean addPassage(Passage passage) throws IllegalArgumentException {
    if (passage == null) {
      throw new IllegalArgumentException("The passage parameter can not be null.");
    }
    String passageTitle = passage.getTitle();
    Link passageLink = new Link(passageTitle,passageTitle);
    if (passages.containsKey(passageLink)) {return false;}
    passages.put(passageLink,passage);
    return true;
  }

  /**
   * Getter method, returns passage from passages hashmap based on the corresponding link.
   *
   * @param link Link corresponding to a passage in the passages HashMap.
   * @return Passage corresponding to the input Link.
   */
  public Passage getPassage(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("The link parameter can not be null.");
    }
    Link passageLink = new Link(link.getReference(), link.getReference());
    return passages.get(passageLink);
  }

  /**
   * Returns all the passages contained within the passages hashmap.
   *
   * @return collection which contains all Passage objects in passages HashMap.
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * Removes a passage from the passages map
   * cannot remove a passage if other passages link to it.
   *
   * @param link Link to the passage set to be removed.
   * @return  returns true if the passage has successfully been removed,
   *          returns false if another passage contains a link to the passage.
   * @throws IllegalArgumentException if link is null.
   */
  public boolean removePassage(Link link) throws IllegalArgumentException{
    if (link == null) throw new IllegalArgumentException("Link parameter must exist");
    boolean linkExists = getPassages()
        .stream()
        .anyMatch(p -> p.getLinks()
            .stream()
            .anyMatch(l -> l.getReference().equals(link.getReference())));
    if (linkExists) {
      return false;
    }
    Link passageLink = new Link(link.getReference(), link.getReference());
    passages.remove(passageLink);
    return true;
  }

  /**
   * Gives a list of link objects that do not point to a passage.
   *
   * @return list of broken links, empty if none.
   */
  public List<Link> getBrokenLinks() {
    Set<Link> uniqueLinks = new HashSet<>();
    passages.values()
        .stream()
        .map(Passage::getLinks)
        .forEach(uniqueLinks::addAll);
    return uniqueLinks
        .stream()
        .filter(link -> getPassages()
            .stream()
            .noneMatch(passage -> passage.getTitle().equals(link.getReference())))
        .toList();
  }
}

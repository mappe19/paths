package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A goal for the player to achieve a given set of items.
 */
public class InventoryGoal implements Goal {
  private final List<String> mandatoryItems;

  /**
   * Constructs a goal with a given set of items.
   *
   * @param mandatoryItems the given set of items as a list of strings.
   * @throws IllegalArgumentException if mandatoryItems is null
   */
  public InventoryGoal(List<String> mandatoryItems) {
    if (mandatoryItems == null) {
      throw new IllegalArgumentException("MandatoryItems parameter can not be null.");
    }
    this.mandatoryItems = new ArrayList<>(mandatoryItems);
  }

  /**
   * Tells you if the mandatory set of items is in the given players inventory.
   *
   * @param player reference to the Player object the goal applies to.
   * @return boolean if the mandatory st of items are in the inventory, and false if not.
   */
  @Override
  public boolean isFulfilled(Player player) {
    HashMap<String, Integer> mandatoryItemsMap = new HashMap<>();
    mandatoryItems.forEach(s -> {
      if (mandatoryItemsMap.containsKey(s)) {
        mandatoryItemsMap.put(s, mandatoryItemsMap.get(s) + 1);
      } else {
        mandatoryItemsMap.put(s, 1);
      }
    });

    HashMap<String, Integer> inventoryMap = new HashMap<>();
    player.getInventory().forEach(s -> {
      if (inventoryMap.containsKey(s)) {
        inventoryMap.put(s, inventoryMap.get(s) + 1);
      } else {
        inventoryMap.put(s, 1);
      }
    });

    int mandatoryItemsFulfilled = 0;
    for (Map.Entry<String, Integer> entry : mandatoryItemsMap.entrySet()) {
      if (inventoryMap.containsKey(entry.getKey())) {
        //Nested if to ensure inventoryMap actually has key
        if (inventoryMap.get(entry.getKey()) >= entry.getValue()) {
          mandatoryItemsFulfilled++;
        }
      }
    }

    return mandatoryItemsFulfilled == mandatoryItemsMap.size();
  }

  /**
   * Returns a text description of the object.
   *
   * @return the description as a String.
   */
  @Override
  public String toString() {
    StringBuilder items = new StringBuilder();
    mandatoryItems.forEach(item -> {
      items.append(item);
      if (mandatoryItems.indexOf(item) != mandatoryItems.size() - 1) {
        items.append(", ");
      } else {
        items.append(".");
      }
    });

    return "Inventory goal, mandatory items: " + items;
  }
}

package no.ntnu.idat2001.mappe19.model;

import no.ntnu.idat2001.mappe19.model.actions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a link between two passages (nodes) of the story.
 */
public class Link {
  private final String text;
  private final String reference;
  private final List<Action> actions;

  /**
   * Constructs a link to a given passage.
   *
   * @param text a string describing the action done in the story to reach the linked passage.
   * @param reference the title of Passage object the constructed link points to.
   */
  public Link(String text, String reference) {
    this.text = text;
    this.reference = reference;
    this.actions = new ArrayList<>();
  }

  /**
   * Returns the text field of the link.
   *
   * @return the text field, a String.
   */
  public String getText() {
    return text;
  }

  /**
   * Returns the reference field of the link.
   *
   * @return the reference field, a String.
   */
  public String getReference() {
    return reference;
  }

  /**
   * Adds an action to be done to player when the link is reached.
   *
   * @param action the reference to the action to be added.
   * @return true if the action is successfully added or false if the Action object already exists in the list of actions.
   * @throws IllegalArgumentException if the action parameter is null
   */
  public boolean addAction(Action action) throws IllegalArgumentException {
    if (action == null) {
      throw new IllegalArgumentException("The action parameter can not be null.");
    }
    for (Action actionInActions : actions) {
      if (action.equals(actionInActions)) {
        return false;
      }
    }
    return actions.add(action);
  }

  /**
   * Returns the list of actions in the link.
   *
   * @return the list of actions.
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * Gives a text description of the link.
   *
   * @return a String describing the link.
   */
  @Override
  public String toString() {
    StringBuilder actionsString = new StringBuilder("\nActions: \n");
    for (Action action : actions) {
      actionsString.append(action).append("\n");
    }
    return "Text: " + text + "\n Reference: " + reference + actionsString;
  }

  /**
   * Compares a Link object to another object, and the tells you
   * if they are equal.
   *
   * @param o the object to be compared.
   * @return true if the object are equals and false if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Link link = (Link) o;
    return Objects.equals(text, link.text) && Objects.equals(reference, link.reference);
  }

  /**
   * Gives a hash of the Link object.
   *
   * @return the hash of the object as an integer.
   */
  @Override
  public int hashCode() {
    return Objects.hash(text, reference, actions);
  }
}

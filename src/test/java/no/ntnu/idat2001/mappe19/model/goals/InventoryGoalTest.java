package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InventoryGoalTest {
  private Player a;

  @BeforeEach
  void init() {
    a = new Player.PlayerBuilder("test")
        .setHealth(50)
        .setScore(50)
        .setGold(100)
        .build();
  }

  @Test
  void testConstructorThrowsExceptionWithNullMandatoryItemsParameter() {
    assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(null));
  }

  @Test
  void testIfInventoryEqualsMandatoryItemsGivesTrueFromIsFulfilled() {
    List<String> mandatoryItems = new ArrayList<>();
    mandatoryItems.add("Shovel");
    mandatoryItems.add("Sword");
    InventoryGoal b = new InventoryGoal(mandatoryItems);
    a.addToInventory("Shovel");
    a.addToInventory("Sword");
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testIfInventoryContainsMandatoryItemsAndOtherItemsGivesTrueFromIsFulfilled() {
    List<String> mandatoryItems = new ArrayList<>();
    mandatoryItems.add("Shovel");
    mandatoryItems.add("Sword");
    InventoryGoal b = new InventoryGoal(mandatoryItems);
    a.addToInventory("Shovel");
    a.addToInventory("Shield");
    a.addToInventory("Axe");
    a.addToInventory("Potion");
    a.addToInventory("Sword");
    assertTrue(b.isFulfilled(a));
  }


  @Test
  void testIfInventoryDoesNotContainAllMandatoryItemsGivesFalseFromIsFulfilled() {
    List<String> mandatoryItems = new ArrayList<>();
    mandatoryItems.add("Shovel");
    mandatoryItems.add("Sword");
    InventoryGoal b = new InventoryGoal(mandatoryItems);
    a.addToInventory("Shovel");
    assertFalse(b.isFulfilled(a));
  }
  @Test
  void testIfMultipleInstancesOfSameMandatoryItemsAndInventoryDoesNotContainAllGivesFalseFromIsFulfilled() {
    List<String> mandatoryItems = new ArrayList<>();
    mandatoryItems.add("Shovel");
    mandatoryItems.add("Sword");
    mandatoryItems.add("Sword");
    InventoryGoal b = new InventoryGoal(mandatoryItems);
    a.addToInventory("Shovel");
    a.addToInventory("Sword");
    assertFalse(b.isFulfilled(a));
  }

  @Test
  void testIfMultipleInstancesOfSameMandatoryItemsAndInventoryContainsAllGivesTrueFromIsFulfilled() {
    List<String> mandatoryItems = new ArrayList<>();
    mandatoryItems.add("Shovel");
    mandatoryItems.add("Sword");
    mandatoryItems.add("Sword");
    InventoryGoal b = new InventoryGoal(mandatoryItems);
    a.addToInventory("Shovel");
    a.addToInventory("Sword");
    a.addToInventory("Sword");
    assertTrue(b.isFulfilled(a));
  }
}
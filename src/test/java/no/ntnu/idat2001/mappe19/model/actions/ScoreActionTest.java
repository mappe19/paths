package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ScoreActionTest {
  private Player p;

  @BeforeEach
  void init() {
    p = new Player.PlayerBuilder("test")
            .setHealth(100)
            .setScore(10)
            .setGold(0)
            .build();
  }

  @Test
  void testPositiveIntegerInConstructorAddsScoreToPlayer() {
    ScoreAction scoreAction = new ScoreAction(10);
    scoreAction.execute(p);
    assertEquals(20, p.getScore());
  }

  @Test
  void testNegativeIntegerInConstructorRemovesScoreFromPlayer() {
    ScoreAction scoreAction = new ScoreAction(-10);
    scoreAction.execute(p);
    assertEquals(0, p.getScore());
  }

  @Test
  void testRemovingMoreScoreThanPlayerHasThrowsSetsScoreToZero() {
    ScoreAction scoreAction = new ScoreAction(-20);
    scoreAction.execute(p);
    assertEquals(0, p.getScore());
  }
}

package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class InventoryActionTest {
  private Player p;

  @BeforeEach
  void init() {
    p = new Player.PlayerBuilder("test")
            .setHealth(100)
            .setScore(10)
            .setGold(0)
            .build();
  }
  @Test
  void testStringInConstructorIsAddedToPlayerInventory() {
    InventoryAction inventoryAction = new InventoryAction("Sword");
    inventoryAction.execute(p);
    ArrayList<String> expected = new ArrayList<>();
    expected.add("Sword");
    assertEquals(expected, p.getInventory());
  }

  @Test
  void testHavingEmptyStringInConstructorDoesNotAddItemToInventory() {
    InventoryAction inventoryAction = new InventoryAction("");
    inventoryAction.execute(p);
    assertFalse(p.getInventory().contains(""));
  }
}

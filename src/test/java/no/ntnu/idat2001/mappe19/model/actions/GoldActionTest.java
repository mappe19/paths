package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoldActionTest {
  private Player p;

  @BeforeEach
  void init() {
    p = new Player.PlayerBuilder("test")
            .setHealth(100)
            .setScore(10)
            .setGold(10)
            .build();
  }

  @Test
  void testPositiveIntegerInConstructorAddsGoldToPlayer() {
    GoldAction goldAction = new GoldAction(10);
    goldAction.execute(p);
    assertEquals(20, p.getGold());
  }

  @Test
  void testNegativeIntegerInConstructorRemovesGoldFromPlayer() {
    GoldAction goldAction = new GoldAction(-10);
    goldAction.execute(p);
    assertEquals(0, p.getGold());
  }

  @Test
  void testRemovingMoreGoldThanPlayerHasSetsGoldToZero() {
    GoldAction goldAction = new GoldAction(-20);
    goldAction.execute(p);
    assertEquals(0, p.getGold());
  }
}
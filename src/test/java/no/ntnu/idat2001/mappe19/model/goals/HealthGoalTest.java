package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HealthGoalTest {
  private Player a;

  @BeforeEach
  void init() {
    a = new Player.PlayerBuilder("test")
        .setHealth(50)
        .setScore(50)
        .setGold(100)
        .build();
  }

  @Test
  void testPlayerHealthAmountEqualsHealthGoalAmountGivesTrueFromIsFulfilled() {
    HealthGoal b = new HealthGoal(50);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerHealthAmountMoreThanHealthGoalAmountGivesTrueFromIsFulfilled() {
    HealthGoal b = new HealthGoal(25);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerHealthAmountLessThanHealthGoalAmountGivesFalseFromIsFulfilled() {
    HealthGoal b = new HealthGoal(100);
    assertFalse(b.isFulfilled(a));
  }
}
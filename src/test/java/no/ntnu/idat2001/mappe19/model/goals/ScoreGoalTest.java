package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreGoalTest {
  private Player a;

  @BeforeEach
  void init() {
    a = new Player.PlayerBuilder("test")
        .setHealth(50)
        .setScore(50)
        .setGold(100)
        .build();
  }

  @Test
  void testPlayerScoreAmountEqualsScoreGoalAmountGivesTrueFromIsFulfilled() {
    ScoreGoal b = new ScoreGoal(50);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerScoreAmountMoreThanScoreGoalAmountGivesTrueFromIsFulfilled() {
    ScoreGoal b = new ScoreGoal(25);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerScoreAmountLessThanScoreGoalAmountGivesFalseFromIsFulfilled() {
    ScoreGoal b = new ScoreGoal(100);
    assertFalse(b.isFulfilled(a));
  }
}
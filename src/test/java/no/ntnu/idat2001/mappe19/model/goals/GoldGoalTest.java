package no.ntnu.idat2001.mappe19.model.goals;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoldGoalTest {
  private Player a;

  @BeforeEach
  void init() {
    a = new Player.PlayerBuilder("test")
        .setHealth(50)
        .setScore(50)
        .setGold(100)
        .build();
  }

  @Test
  void testPlayerGoldAmountEqualsGoldGoalAmountGivesTrueFromIsFulfilled() {
    GoldGoal b = new GoldGoal(100);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerGoldAmountMoreThanGoldGoalAmountGivesTrueFromIsFulfilled() {
    GoldGoal b = new GoldGoal(50);
    assertTrue(b.isFulfilled(a));
  }

  @Test
  void testPlayerGoldAmountLessThanGoldGoalAmountGivesFalseFromIsFulfilled() {
    GoldGoal b = new GoldGoal(150);
    assertFalse(b.isFulfilled(a));
  }
}
package no.ntnu.idat2001.mappe19.model.actions;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HealthActionTest {
  private Player p;

  @BeforeEach
  void init() {
    p = new Player.PlayerBuilder("test")
            .setHealth(100)
            .setScore(10)
            .setGold(0)
            .build();
  }

  @Test
  void testPositiveIntegerInConstructorAddsHealthToPlayer() {
    HealthAction healthAction = new HealthAction(10);
    healthAction.execute(p);
    assertEquals(110, p.getHealth());
  }

  @Test
  void testNegativeIntegerInConstructorRemovesHealthFromPlayer() {
    HealthAction healthAction = new HealthAction(-10);
    healthAction.execute(p);
    assertEquals(90, p.getHealth());
  }

  @Test
  void testRemovingMoreHealthThanPlayerSetsHealthToZero() {
    HealthAction healthAction = new HealthAction(-120);
    healthAction.execute(p);
    assertEquals(0, p.getHealth());
  }
}

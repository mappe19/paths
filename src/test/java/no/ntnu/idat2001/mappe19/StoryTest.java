package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StoryTest {
  private Passage openingPassage;
  private Story testStory;
  private Passage testPassage;

  @BeforeEach
  void init() {
    openingPassage = new Passage("Start", "Content");
    testStory = new Story("Story", openingPassage);
    testPassage = new Passage("Nr 1", "More Content");
  }

  @Nested
  class testAddPassage {
    @Test
    void testAddPassageToEmptyMapReturnsTrue() {
      assertTrue(testStory.addPassage(testPassage));
    }

    @Test
    void testAddNewPassageToNotEmptyPassageMapReturnsTrue() {
      Passage passage1 = new Passage("Nr 1", "More Content");
      Passage passage2 = new Passage("Nr 2", "Even more content");
      testStory.addPassage(passage1);
      assertTrue(testStory.addPassage(passage2));
    }

    @Test
    void testAddPassageAlreadyInMapReturnsFalse() {
      testStory.addPassage(testPassage);
      assertFalse(testStory.addPassage(testPassage));
    }
  }

  @Test
  void testGetPassageReturnsCorrectPassageWithLinkFormattedAsDoneInAddPassage() {
    Link testPassageKey = new Link("Nr 1", "Nr 1");
    testStory.addPassage(testPassage);
    assertEquals(testPassage, testStory.getPassage(testPassageKey));
  }

  @Nested
  class testRemovePassage {
    @Test
    void testRemovePassageWhenLinkExistsInAnotherPassage() {
      Link testPassageKey = new Link("Nr 1", "Nr 1");
      testStory.addPassage(testPassage);
      Passage passage2 = new Passage("Nr 2", "Nr 2");
      passage2.addLink(testPassageKey);
      testStory.addPassage(passage2);
      assertFalse(testStory.removePassage(testPassageKey));
    }

    @Test
    void testRemovePassageWhenNoOtherLinkExists() {
      Link testPassageKey = new Link("Nr 1", "Nr 1");
      testStory.addPassage(testPassage);
      assertTrue(testStory.removePassage(testPassageKey));
    }

    @Test
    void testRemovePassageThrowsExceptionWithNullLinkParameter() {
      assertThrows(IllegalArgumentException.class, () -> testStory.removePassage(null));
    }
  }

  @Nested
  class testGetBrokenLinks {
    @Test
    void testGetBrokenLinksReturnsCorrectLinksWhenNotBrokenLinkIsEqualToKey() {
      Link brokenLink = new Link("test", "test");
      Link notBrokenLink = new Link("Start", "Start");
      testPassage.addLink(brokenLink);
      testPassage.addLink(notBrokenLink);
      testStory.addPassage(testPassage);
      List<Link> expectedResult = new ArrayList<>();
      expectedResult.add(brokenLink);
      assertEquals(expectedResult, testStory.getBrokenLinks());
    }

    @Test
    void testGetBrokenLinksReturnsCorrectLinksWhenNotBrokenLinkIsNotEqualToKey() {
      Link brokenLink = new Link("test", "test");
      Link notBrokenLink = new Link("Go to start", "Start");
      testPassage.addLink(brokenLink);
      testPassage.addLink(notBrokenLink);
      testStory.addPassage(testPassage);
      List<Link> expectedResult = new ArrayList<>();
      expectedResult.add(brokenLink);
      assertEquals(expectedResult, testStory.getBrokenLinks());
    }
  }
}
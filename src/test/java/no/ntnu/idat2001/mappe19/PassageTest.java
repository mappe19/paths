package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PassageTest {
  private Passage p;

  @BeforeEach
  void init() {
    p = new Passage("Mountain", "There is a mountain");
  }

  @Nested
  class testAddLink {
    @Test
    void testAddNewLinkToEmptyListReturnsTrue() {
      Link link = new Link("Walk away from mountain", "Forest");
      assertTrue(p.addLink(link));
    }

    @Test
    void testAddLinkThatAlreadyExistsReturnsFalse() {
      Link forestLink1 = new Link("Walk away from mountain", "Forest");
      Link forestLink2 = new Link("Walk away from mountain", "Forest");
      p.addLink(forestLink1);
      assertFalse(p.addLink(forestLink2));
    }

    @Test
    void testAddLinkThrowsExceptionWithNullLinkParameter() {
      assertThrows(IllegalArgumentException.class, () -> p.addLink(null));
    }
  }

  @Nested
  class testHasLinks {
    @Test
    void testHasLinksReturnsFalseWhenThereIsNoLinks() {
      assertFalse(p.hasLinks());
    }

    @Test
    void testHasLinksReturnsTrueWhenThereAreLinks() {
      p.addLink(new Link("Walk away from mountain", "Forest"));
      assertTrue(p.hasLinks());
    }
  }

  @Nested
  class testEquals {
    private Passage p1;
    private Passage p2;
    private Passage p3;

    @BeforeEach
    void init() {
      p1 = new Passage("Mountain", "There is a mountain");
      p2 = new Passage("Mountain", "There is a mountain");
      p3 = new Passage("Forest", "There is a forest");
    }

    @Test
    void testEqualsReturnsTrueOnTheSameReference() {
      assertEquals(p1, p1);
    }

    @Test
    void testEqualsReturnsTrueOnDifferentButAlikeObjects() {
      assertEquals(p1, p2);
    }

    @Test
    void testEqualsReturnsFalseOnDifferentObjects() {
      assertNotEquals(p1, p3);
    }
  }
}
package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.model.*;
import no.ntnu.idat2001.mappe19.model.goals.Goal;
import no.ntnu.idat2001.mappe19.model.goals.GoldGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
  private Game game;
  private Player player;
  private Goal goal;
  private List<Goal> goals;
  private Passage passage;
  private Story story;

  @BeforeEach
  void init() {
    player = new Player.PlayerBuilder("test")
            .setHealth(100)
            .setScore(100)
            .setGold(100)
            .build();
    goal = new GoldGoal(10);
    goals = new ArrayList<>();
    goals.add(goal);
    passage = new Passage("Mountain", "There is a mountain");
    story = new Story("Mountain story", passage);
    game = new Game(player, story, goals);
  }

  @Nested
  class testConstructor {
    @Test
    void testConstructorThrowsExceptionWithNullPlayerParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Game(null, story, goals));
    }

    @Test
    void testConstructorThrowsExceptionWithNullStoryParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, null, goals));
    }

    @Test
    void testConstructorThrowsExceptionWithNullGameGoalsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, story, null));
    }
  }

  @Test
  void testBeginReturnsGivenOpeningPassage() {
    assertEquals(passage, game.begin());
  }

  @Test
  void testGoReturnsPassageCorrespondingToGivenLink() {
    Link link = new Link("Mountain", "Mountain");
    assertEquals(passage, game.go(link));
  }
}
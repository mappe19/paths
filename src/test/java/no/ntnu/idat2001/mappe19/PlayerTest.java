package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
  private Player p;

  @BeforeEach
  void init() {
    p = new Player.PlayerBuilder("test")
            .setGold(100)
            .setHealth(100)
            .setScore(100)
            .build();
  }

  @Nested
  class testAddHealth {
    @Test
    void testAddHealthWithPositiveIntegerIncreasesPlayerHealth() {
      p.addHealth(10);
      assertEquals(110, p.getHealth());
    }

    @Test
    void testAddHealthWithNegativeIntegerDecreasesPlayerHealth() {
      p.addHealth(-10);
      assertEquals(90, p.getHealth());
    }

    @Test
    void testAddHealthWithNegativeIntegerMoreThanPlayerHealthThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> p.addHealth(-110));
    }
  }

  @Nested
  class testAddScore {
    @Test
    void testAddScoreWithPositiveIntegerIncreasesPlayerScore() {
      p.addScore(10);
      assertEquals(110, p.getScore());
    }

    @Test
    void testAddScoreWithNegativeIntegerDecreasesPlayerScore() {
      p.addScore(-10);
      assertEquals(90, p.getScore());
    }

    @Test
    void testAddScoreWithNegativeIntegerMoreThanPlayerScoreThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> p.addScore(-110));
    }
  }

  @Nested
  class testAddGold {
    @Test
    void testAddGoldWithPositiveIntegerIncreasesPlayerGoldAmount() {
      p.addGold(10);
      assertEquals(110, p.getGold());
    }

    @Test
    void testAddGoldWithNegativeIntegerDecreasesPlayerGoldAmount() {
      p.addGold(-10);
      assertEquals(90, p.getGold());
    }

    @Test
    void testAddScoreWithNegativeIntegerMoreThanPlayerScoreThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> p.addGold(-110));
    }
  }

  @Nested
  class testAddToInventory {
    @Test
    void testAddToInventoryAddsStringToPlayerInventory() {
      p.addToInventory("Sword");
      ArrayList<String> expected = new ArrayList<>();
      expected.add("Sword");
      assertEquals(expected, p.getInventory());
    }

    @Test
    void testAddToInventoryEmptyStringThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> p.addToInventory(""));
    }
  }

  @Nested
  class testBuilderReturnsObjectCorrectlyWhenOnlySomeAttributesAreSet {
    @Test
    void testBuilderReturnsCorrectObjectWhenOnlyNameIsSet() {
      Player testPlayer = new Player.PlayerBuilder("test").build();

      assertEquals("test", testPlayer.getName());
      assertEquals(0, testPlayer.getHealth());
      assertEquals(0, testPlayer.getScore());
      assertEquals(0, testPlayer.getGold());
    }

    @Test
    void testBuilderReturnsCorrectObjectWhenNameAndHealthAreSet() {
      Player testPlayer = new Player.PlayerBuilder("test")
              .setHealth(100)
              .build();

      assertEquals("test", testPlayer.getName());
      assertEquals(100, testPlayer.getHealth());
      assertEquals(0, testPlayer.getScore());
      assertEquals(0, testPlayer.getGold());
    }

    @Test
    void testBuilderReturnsCorrectObjectWhenAllAttributesAreSet() {
      Player testPlayer = new Player.PlayerBuilder("test")
              .setHealth(100)
              .setScore(100)
              .setGold(100)
              .build();

      assertEquals("test", testPlayer.getName());
      assertEquals(100, testPlayer.getHealth());
      assertEquals(100, testPlayer.getScore());
      assertEquals(100, testPlayer.getGold());
    }
  }
}
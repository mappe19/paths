package no.ntnu.idat2001.mappe19;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.actions.Action;
import no.ntnu.idat2001.mappe19.model.actions.GoldAction;
import no.ntnu.idat2001.mappe19.model.actions.HealthAction;
import no.ntnu.idat2001.mappe19.model.actions.ScoreAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkTest {

  private Link a;

  @BeforeEach
  void init() {
    a = new Link("This is a test link", "Test");
  }

  @Nested
  class testAddAction {
    @Test
    void testAddActionToEmptyActionListReturnsTrue() {
      Action b = new GoldAction(55);
      assertTrue(a.addAction(b));
    }

    @Test
    void testAddNewActionToNotEmptyActionListReturnsTrue() {
      Action b = new ScoreAction(10);
      Action c = new ScoreAction(20);
      a.addAction(b);
      assertTrue(a.addAction(c));
    }

    @Test
    void testAddSameActionToListTwiceReturnsFalseSecondTime() {
      Action b = new HealthAction(20);
      a.addAction(b);
      assertFalse(a.addAction(b));
    }

    @Test
    void testAddActionThrowsExceptionWithNullActionParameter() {
      assertThrows(IllegalArgumentException.class, () -> a.addAction(null));
    }
  }

  @Nested
  class testEquals {
    @Test
    void testEqualsReturnsTrueOnSameReference() {
      Link b = a;
      assertEquals(a, b);
    }

    @Test
    void testEqualsReturnsTrueOnTwoDifferentButAlikeObjects() {
      Link b = new Link("This is a test link", "Test");
      assertEquals(a, b);
    }

    @Test
    void testEqualsReturnsFalseOnTwoDifferentObjects() {
      Link b = new Link("A Link", "Testing");
      assertNotEquals(a, b);
    }
  }
}
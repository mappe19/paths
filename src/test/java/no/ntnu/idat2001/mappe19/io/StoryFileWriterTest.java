package no.ntnu.idat2001.mappe19.io;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.actions.GoldAction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class StoryFileWriterTest {
  private Story story;
  private Passage passage;
  private Link link;
  private GoldAction action;

  @BeforeEach
  void init() {
    action = new GoldAction(10);
    link = new Link("Go to forest.", "Forest");
    link.addAction(action);
    passage = new Passage("Mountain", "There is a mountain");
    passage.addLink(link);
    story = new Story("Short story.", passage);
  }

  @AfterEach
  void cleanUp() {
    File fileToDelete = new File(".\\test.paths");
    if (fileToDelete.delete()) {
      System.out.println("Test file deleted.");
    } else {
      System.out.println("Test file never created.");
    }
  }

  @Test
  void testWriteToFileActuallyWritesToFile() {
    try {
      StoryFileWriter.writeToFile(".\\test.paths", story);
    } catch (IOException e) {
      System.out.println("Error writing to file.");
      e.printStackTrace();
    }
    File file = new File(".\\test.paths");
    assertTrue(file.exists());
  }

  @Test
  void testWriteToFileCannotWriteToNonPathsFile() {
    String message = "";
    try {
      StoryFileWriter.writeToFile(".\\test.txt", story);
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("File must end with .paths", message);
  }

  @Test
  void testWriteToFileThrowsWhenStoryIsNull() {
    assertThrows(IllegalArgumentException.class, () -> StoryFileWriter.writeToFile(".\\test.paths", null));
  }

  @Test
  void testWriteToFileWritesCorrectFormat() {
    try {
      StoryFileWriter.writeToFile(".\\test.paths", story);
    } catch (IOException e) {
      System.out.println("Error writing to file.");
      e.printStackTrace();
    }
    String content = "";
    try {
      content = Files.readString(Path.of(".\\test.paths"));
    } catch (IOException e) {
      System.out.println("Error reading file.");
      e.printStackTrace();
    }
    String expected = "Short story.\n\n::Mountain\nThere is a mountain\n(Go to forest.)[Forest]\n- gold_action, value: 10\n\n";
    assertEquals(expected, content);
  }

  @Test
  void testWriteToFileWritesCorrectFormatWhenMultiplePassages() {
    Passage newPassage = new Passage("Forest", "There is a forest.");
    story.addPassage(newPassage);
    try {
      StoryFileWriter.writeToFile(".\\test.paths", story);
    } catch (IOException e) {
      System.out.println("Error writing to file.");
      e.printStackTrace();
    }
    String content = "";
    try {
      content = Files.readString(Path.of(".\\test.paths"));
    } catch (IOException e) {
      System.out.println("Error reading file.");
      e.printStackTrace();
    }
    String expected = """
        Short story.

        ::Mountain
        There is a mountain
        (Go to forest.)[Forest]
        - gold_action, value: 10

        ::Forest
        There is a forest.

        """;
    assertEquals(expected, content);
  }
}
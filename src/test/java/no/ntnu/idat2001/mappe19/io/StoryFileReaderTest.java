package no.ntnu.idat2001.mappe19.io;

import no.ntnu.idat2001.mappe19.model.Link;
import no.ntnu.idat2001.mappe19.model.Passage;
import no.ntnu.idat2001.mappe19.model.Story;
import no.ntnu.idat2001.mappe19.model.actions.GoldAction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class StoryFileReaderTest {
  private Story story;
  private Passage passage;
  private Link link;
  private GoldAction action;

  @BeforeEach
  void init() {
    action = new GoldAction(10);
    link = new Link("Go to forest.", "Forest");
    link.addAction(action);
    passage = new Passage("Mountain", "There is a mountain");
    passage.addLink(link);
    story = new Story("Short story.", passage);
  }

  @AfterEach
  void cleanUp() {
    File fileToDelete = new File(".\\test.paths");
    if (fileToDelete.delete()) {
      System.out.println("Test file deleted.");
    } else {
      System.out.println("Test file never created.");
    }
  }

  @Test
  void testReadFromFileSuccessfullyReadsFromFileWhenFormatIsCorrect() {
    try {
      StoryFileWriter.writeToFile(".\\test.paths", story);
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    Story returnedStory = null;
    try {
      returnedStory = StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      System.out.println("Something went wrong reading from file.");
      System.out.println(e.getMessage());
    }
    assertNotNull(returnedStory);
    assertEquals(story.getTitle(), returnedStory.getTitle());
    assertEquals(story.getOpeningPassage(), returnedStory.getOpeningPassage());
    assertEquals(story.getOpeningPassage().getLinks().get(0),
        returnedStory.getOpeningPassage().getLinks().get(0));
    assertEquals(story.getOpeningPassage().getLinks().get(0).getActions().get(0).toString(),
        returnedStory.getOpeningPassage().getLinks().get(0).getActions().get(0).toString());
  }

  @Test
  void testReadFromFileSuccessfullyReadsFromFileWhenFormatIsCorrectAndStoryHasMultiplePassages() {
    Passage newPassage = new Passage("Forest", "There is a forest.");
    story.addPassage(newPassage);
    try {
      StoryFileWriter.writeToFile(".\\test.paths", story);
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    Story returnedStory = null;
    try {
      returnedStory = StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      System.out.println("Something went wrong reading from file.");
      System.out.println(e.getMessage());
    }
    assertNotNull(returnedStory);
    assertEquals(story.getTitle(), returnedStory.getTitle());
    assertEquals(story.getOpeningPassage(), returnedStory.getOpeningPassage());
    assertEquals(story.getOpeningPassage().getLinks().get(0),
        returnedStory.getOpeningPassage().getLinks().get(0));
    assertEquals(story.getOpeningPassage().getLinks().get(0).getActions().get(0).toString(),
        returnedStory.getOpeningPassage().getLinks().get(0).getActions().get(0).toString());
    assertEquals(story.getPassage(new Link("Forest", "Forest")),
        returnedStory.getPassage(new Link("Forest", "Forest")));
  }

  @Test
  void testReadFromFileThrowsCorrectWhenFileEndingIsWrong() {
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.txt");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("The provided file does not have the .paths filetype.", message);
  }

  @Test
  void testReadFromFileThrowsCorrectWhenFileDoesNotExist() {
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("Unable to read from given file.", message);
  }

  @Test
  void testReadFromFileThrowsCorrectWhenFileIsEmpty() {
    try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
      fileWriter.write("");
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("The file is empty.", message);
  }

  @Test
  void testReadFromFileThrowsCorrectWhenFileHasNoPassages() {
    try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
      fileWriter.write("name\n\n");
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("The story has no passages.", message);
  }

  @Nested
  class testFileHeaderFormattingExceptions {
    @Test
    void testReadFromFileThrowsCorrectWhenFileHeaderHasNoNewLineBeforeFirstPassage() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name::passage");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("There is no newline after the name of the story.", message);
    }

    @Test
    void testReadFromFileThrowsCorrectWhenFileHeaderHasNotEmptyLineAfterStoryName() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name\nlol");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("The formatting between the story name" +
          " and the first passage is wrong.", message);
    }

    @Test
    void testReadFromFileThrowsCorrectWhenFileHeaderDoesNotExist() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("::lol");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("The file does not start with the header.", message);
    }
  }

  @Nested
  class testPassageFormattingExceptions {
    @Test
    void testReadFromFileThrowsCorrectWhenPassageIsNotFullyDefined() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name\n\n::lol\n\n");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("Error parsing passage nr. 1.", message);
    }

    @Test
    void testReadFromFileThrowsCorrectWhenPassageBlockIsNotProperlyFormatted() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name\n\n::lol");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("Passage block did not end with empty line.", message);
    }

    @Test
    void testReadFromFileThrowsCorrectWhenPassageHasIllegalFormatting() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name\n\n::lol\nhello\n+\n\n");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("An illegal line was found while parsing passage nr. 1.", message);
    }

    @Test
    void testReadFromFileThrowsCorrectWhenAnActionIsDefinedBeforeALink() {
      try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
        fileWriter.write("name\n\n::lol\nhello\n- action\n\n");
      } catch (Exception e) {
        System.out.println("Something went wrong writing to file.");
        e.printStackTrace();
      }
      String message = "";
      try {
        StoryFileReader.readFromFile(".\\test.paths");
      } catch (IOException e) {
        message = e.getMessage();
      }
      assertEquals("An action was specified before a link in passage nr 1.", message);
    }
  }

  @Test
  void testReadFromFileThrowsCorrectWhenLinkIsNotFullyDefined() {
    try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
      fileWriter.write("name\n\n::lol\nhello\n(lol)[]\n\n");
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("One of the links being added to passage nr. 1 was wrong.", message);
  }

  @Test
  void testReadFromFileThrowsCorrectWhenActionIsNotFullyDefined() {
    try (FileWriter fileWriter = new FileWriter(".\\test.paths")) {
      fileWriter.write("name\n\n::lol\nhello\n(lol)[lol]\n-\n\n");
    } catch (Exception e) {
      System.out.println("Something went wrong writing to file.");
      e.printStackTrace();
    }
    String message = "";
    try {
      StoryFileReader.readFromFile(".\\test.paths");
    } catch (IOException e) {
      message = e.getMessage();
    }
    assertEquals("One of the actions being added to a link in passage nr 1 was wrong.", message);
  }
}